/*  202410233.231453: dwrr
  a shell / editor application 
  that doesnt use the terminal at all.
rewritten on 2412161.200916

*/
#include <time.h>
#include <sys/time.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <iso646.h>
#include <stdbool.h>
#include <stdint.h>
#include <iso646.h>
#include <string.h>
#include <assert.h>

#include <SDL.h>

typedef uint64_t nat;
typedef uint8_t byte;

static byte font[(27 + 4) * 9] = {
0,0,0,0,0,0,0,0,0, 			// space
0,0,0,62,1,63,65,65,62,			// a
64,64,64,64,126,65,65,65,62,		// b
0,0,0,0,62,64,64,64,62,			// c
1,1,1,1,63,65,65,65,62,			// d
0,0,30,17,65,127,64,65,62,		// e
0,15,16,16,60,16,16,16,16,		// f
0,61,67,65,65,63,1,65,62,		// g
64,64,64,126,65,65,65,65,65,		// h
0,8,0,56,8,8,8,8,127,			// i
0,8,0,24,8,8,8,8,112,			// j
64,68,72,112,112,72,68,66,65,
56,8,8,8,8,8,8,8,7,
0,0,0,42,93,73,73,65,65,
0,0,0,93,97,65,65,65,65,
0,0,0,0,62,65,65,65,62,
0,93,97,65,65,126,64,64,64,
0,29,67,65,65,63,1,1,1,

0,94,97,64,64,64,64,64,64,		// r
  0,  0, 62, 65, 64, 62,  1, 65, 62, // s
  8,  8, 30,  8,  8,  8,  8,  8,  7, // t
  0,  0,  0, 65, 65, 65, 65, 67, 29, // u
  0,  0,  0, 65, 65, 34, 34, 20, 8,  // v 
  0,  0,  0, 65, 65, 73, 73, 93, 42, // w
  0,  0, 65, 34, 20,  8, 20, 34, 65, // x
  0,  0,  0, 65, 34, 20,  8, 16, 96, // y
  0,  0,127,  2,  4,  8, 16, 32,127, // z
  0,  0,  0,  0,  0,  0,  0,  0,  8, // .
  0,  0,  0,  0,  0,  0,  0,  8,  16, // ,
  0,  0,  0,  0,  127,0,  0,  0,  0, // -

0,0,0,0,0,0,0,0,0, 		// nl
};


int main(void) {
	srand(42);

	char* text = strdup("this is my cool string\nwhich i want to display.");
	nat text_length = strlen(text);
	nat cursor = text_length;
	nat anchor = 0;
	bool quit = false, resized = false;
	SDL_Surface* surface;
	size_t pixel_count = 0;
	nat row = 0, column = 0, lpixel_size = 4;
	nat row_count = 0, column_count = 0;
	nat key_pressed[256] = {0};
	int window_width = 1600, window_height = 1000;
	bool fullscreen = false;

	if (SDL_Init(SDL_INIT_VIDEO)) abort();	
	SDL_Window *window = SDL_CreateWindow(
		" ", 
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 
		window_width, window_height,
		SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI
	);
	SDL_ShowCursor(SDL_DISABLE);
	SDL_GetWindowSize(window, &window_width, &window_height);
	surface = SDL_GetWindowSurface(window);
	pixel_count = (size_t) surface->w * (size_t) surface->h;


	while (not quit) {
		SDL_Event event;
		if (SDL_WaitEvent(&event)) {
			const Uint8* key = SDL_GetKeyboardState(0);
			if (event.type == SDL_QUIT) { quit = true; }
			else if (event.type == SDL_WINDOWEVENT) {
				if (	event.window.event == SDL_WINDOWEVENT_RESIZED or 
					event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) resized = true;
			} else if (event.type == SDL_MOUSEMOTION) {
				SDL_ShowCursor(SDL_ENABLE);

			} else if (event.type == SDL_KEYDOWN) {
				if (key[SDL_SCANCODE_SLASH]) {
					SDL_SetWindowFullscreen(window, not fullscreen ? SDL_WINDOW_FULLSCREEN : 0); 
					fullscreen = not fullscreen;
					resized = true;
				}

				const bool shift = key[SDL_SCANCODE_LSHIFT] or key[SDL_SCANCODE_RSHIFT];

				if (key[SDL_SCANCODE_Q] and shift) quit = true; 

				if (key[SDL_SCANCODE_A] and shift) {
					if (lpixel_size > 1) lpixel_size -= 1;
					resized = true;
				}

				if (key[SDL_SCANCODE_S] and shift) {
					if (lpixel_size < 400) lpixel_size += 1;
					resized = true;
				}
			}
			

			int convert_keys[] = { 
				SDL_SCANCODE_BACKSPACE, 127, 
				SDL_SCANCODE_SPACE, ' ',
				SDL_SCANCODE_A, 'a', 
				SDL_SCANCODE_T, 'b', 
				SDL_SCANCODE_V, 'c', 

				SDL_SCANCODE_W, 'd', 
				SDL_SCANCODE_K, 'e', 
				SDL_SCANCODE_U, 'f', 
				SDL_SCANCODE_G, 'g', 
				SDL_SCANCODE_D, 'h', 

				SDL_SCANCODE_SEMICOLON, 'i', 
				SDL_SCANCODE_Y, 'j', 
				SDL_SCANCODE_N, 'k', 
				SDL_SCANCODE_M, 'l', 
				SDL_SCANCODE_C, 'm', 

				SDL_SCANCODE_J, 'n', 
				SDL_SCANCODE_L, 'o', 
				SDL_SCANCODE_O, 'p', 
				SDL_SCANCODE_Q, 'q', 
				SDL_SCANCODE_E, 'r', 

				SDL_SCANCODE_S, 's', 
				SDL_SCANCODE_F, 't', 
				SDL_SCANCODE_I, 'u', 
				SDL_SCANCODE_B, 'v', 
				SDL_SCANCODE_R, 'w', 

				SDL_SCANCODE_X, 'x', 
				SDL_SCANCODE_H, 'y', 
				SDL_SCANCODE_Z, 'z', 
				SDL_SCANCODE_PERIOD, '.', 
				SDL_SCANCODE_COMMA, ',', 

				SDL_SCANCODE_MINUS, '-', 
				SDL_SCANCODE_RETURN, '\n', 
				SDL_SCANCODE_TAB, '\t', 
			};


			int c = 0;
			
			for (int i = 0; i < 2 * 33; i += 2) {
				c = convert_keys[i + 1];
				if (not key[convert_keys[i]]) { key_pressed[c] = 0; continue; }
				
				if (key_pressed[c]) {
					key_pressed[c]++;
					if (key_pressed[c] < (c == 127 ? 3 : 12)) continue;
				} else key_pressed[c] = 1;

				SDL_ShowCursor(SDL_DISABLE);

				nat character = 1;
				if (c >= 'a' and c <= 'z') character = (nat) (c - 'a') + 1;
				if (c == ' ' or c == 127) character = 0;
				if (c == '.' ) character = 1 + 26 + 0; 
				if (c == ',' ) character = 1 + 26 + 1; 
				if (c == '-' ) character = 1 + 26 + 2;
				if (c == '\n') character = 1 + 26 + 3;

				SDL_LockSurface(surface);
				nat rowi = 0, columni = 0;
				for (nat fr = 0; fr < 10; fr++) {
					for (nat fc = 8; fc--; ) {
						uint32_t color = 0;
						if (fr < 9) color = (uint32_t) ((font[9 * character + fr] & (1 << fc)) ? -1 : 0);
						const nat width = (nat) surface->w;
						for (nat r = 0; r < lpixel_size; r++) {
							for (nat cc = 0; cc < lpixel_size; cc++) {
								const nat at = width * (lpixel_size * (row + rowi) + r) 
									+ (lpixel_size * (column + columni) + cc);
								((uint32_t*) surface->pixels)[at] = (uint32_t) color;
							}
						}
						columni++;
					}
					columni = 0; rowi++;
				}
				SDL_UnlockSurface(surface);
				SDL_UpdateWindowSurface(window);

				if (c == '\n') {
					column = 0;
					if (row >= row_count - 1) row = 0; else row += 10;
				} else if (c != 127) {
					if (column >= column_count - 1) { 
						column = 0; 
						if (row >= row_count - 1) row = 0; else row += 10;
					} else column += 8;
				}
			}
		}
		
		if (resized) {
			resized = 0;
			SDL_GetWindowSize(window, &window_width, &window_height);
			surface = SDL_GetWindowSurface(window);
			pixel_count = (size_t)surface->w * (size_t)surface->h;
			row = 0; column = 0; 
			row_count = (nat) surface->h / lpixel_size - 1;
			column_count = (nat) surface->w / lpixel_size - 1;
			SDL_LockSurface(surface);
			memset((uint32_t*) surface->pixels, 0, sizeof(uint32_t) * pixel_count);
			
			row = 0; column = 0;
			nat origin = 0;
			for (nat char_pos = origin; char_pos < text_length; char_pos++) {

				const char c = text[char_pos];

				if (c == 10) {

				column = 0;
					if (row >= row_count - 1) row = 0; else row += 10;
					
				} else {

					nat character = 1;
					if (c >= 'a' and c <= 'z') character = (nat) (c - 'a') + 1;
					if (c == ' ' or c == 127) character = 0;
					if (c == '.' ) character = 1 + 26 + 0; 
					if (c == ',' ) character = 1 + 26 + 1; 
					if (c == '-' ) character = 1 + 26 + 2;
					if (c == '\n') character = 1 + 26 + 3;

					nat rowi = 0, columni = 0;
					for (nat fr = 0; fr < 10; fr++) {
						for (nat fc = 8; fc--; ) {
							uint32_t color = 0;
							if (fr < 9) color = (uint32_t) ((font[9 * character + fr] & (1 << fc)) ? -1 : 0);
							const nat width = (nat) surface->w;
							for (nat r = 0; r < lpixel_size; r++) {
								for (nat cc = 0; cc < lpixel_size; cc++) {
									const nat at = width * (lpixel_size * (row + rowi) + r) 
										+ (lpixel_size * (column + columni) + cc);
									((uint32_t*) surface->pixels)[at] = (uint32_t) color;
								}
							}
							columni++;
						}
						columni = 0; rowi++;
					}

					if (column >= column_count - 1) { 
						column = 0; 
						if (row >= row_count - 1) row = 0; else row += 10;
					} else column += 8;
				}
			}
			SDL_UnlockSurface(surface);
			SDL_UpdateWindowSurface(window);
		}
	}
	SDL_DestroyWindow(window);
	SDL_Quit();
}

























































/*


	nat rowi = 0, columni = 0;
	for (nat fr = 0; fr < 10; fr++) {
		for (nat fc = 8; fc--; ) {
			const nat width = (nat) surface->w;
			for (nat r = 0; r < lpixel_size; r++) {
				for (nat c = 0; c < lpixel_size; c++) {
					const nat at = width * (lpixel_size * (row + rowi) + r) + (lpixel_size * (column + columni) + c);
					((uint32_t*) surface->pixels)[at] = (uint32_t) 0;
				}
			}
			columni++;
		}
		columni = 0; rowi++;
	}


	if (cs == 127) {
		if (column < 8) { 
			column = column_count - 1; 
			if (row >= 10) row -= 10;
		} else column -= 8;
	}
	SDL_LockSurface(surface);

	rowi = 0; columni = 0;
	for (nat fr = 0; fr < 10; fr++) {
		for (nat fc = 8; fc--; ) {
			uint32_t color = 0;
			if (fr < 9) color = (uint32_t) ((font[9 * character + fr] & (1 << fc)) ? -1 : 0);
			const nat width = (nat) surface->w;
			for (nat r = 0; r < lpixel_size; r++) {
				for (nat c = 0; c < lpixel_size; c++) {
					const nat at = width * (lpixel_size * (row + rowi) + r) + (lpixel_size * (column + columni) + c);
					((uint32_t*) surface->pixels)[at] = (uint32_t) color;
				}
			}
			columni++;
		}
		columni = 0; rowi++;
	}
	if (cs == '\n') {
		column = 0;
		if (row >= row_count - 1) row = 0; else row += 10;
	} else if (cs != 127) {
		if (column >= column_count - 1) { 
			column = 0; 
			if (row >= row_count - 1) row = 0; else row += 10;
		} else column += 8;
	}

	rowi = 0; columni = 0;
	for (nat fr = 0; fr < 10; fr++) {
		for (nat fc = 8; fc--; ) {
			const nat width = (nat) surface->w;
			for (nat r = 0; r < lpixel_size; r++) {
				for (nat c = 0; c < lpixel_size; c++) {
					const nat at = width * (lpixel_size * (row + rowi) + r) + (lpixel_size * (column + columni) + c);
					((uint32_t*) surface->pixels)[at] = (uint32_t) -1;
				}
			}
			columni++;
		}
		columni = 0; rowi++;
	}
	SDL_UnlockSurface(surface);
	skip_insert:;
	SDL_ShowCursor(SDL_DISABLE);




			skip_over:;



*/













/*


















if (key[SDL_SCANCODE_BACKSPACE]) { print_char(127); } else key_pressed[127] = false;
			if (key[SDL_SCANCODE_SPACE]) { cs = ' '; print_char(' '); } else key_pressed[' '] = false;
			if (key[SDL_SCANCODE_A]) { cs = 'a'; print_char('a'); } else key_pressed['a'] = false;





	row = 0; column = 0; 
	lpixel_size = 4;
	row_count = (nat) surface->h / lpixel_size - 1;
	column_count = (nat) surface->w / lpixel_size - 1;
	SDL_LockSurface(surface);
 	nat rowi = 0; nat columni = 0;
	for (nat fr = 0; fr < 10; fr++) {
		for (nat fc = 8; fc--; ) {
			const nat width = (nat) surface->w;
			for (nat r = 0; r < lpixel_size; r++) {
				for (nat c = 0; c < lpixel_size; c++) {
					const nat at = width * (lpixel_size * (row + rowi) + r) + (lpixel_size * (column + columni) + c);
					((uint32_t*) surface->pixels)[at] = (uint32_t) -1;
				}
			}
			columni++;
		}
		columni = 0; rowi++;
	}
	SDL_UnlockSurface(surface);
	int cs = 0;



















			if (key[SDL_SCANCODE_T]) { cs = 'b'; print_char('b'); } else key_pressed['b'] = false;
			if (key[SDL_SCANCODE_V]) { cs = 'c'; print_char('c'); } else key_pressed['c'] = false;
			if (key[SDL_SCANCODE_W]) { cs = 'd'; print_char('d'); } else key_pressed['d'] = false;
			if (key[SDL_SCANCODE_K]) { cs = 'e'; print_char('e'); } else key_pressed['e'] = false;
			if (key[SDL_SCANCODE_U]) { cs = 'f'; print_char('f'); } else key_pressed['f'] = false;
			if (key[SDL_SCANCODE_G]) { cs = 'g'; print_char('g'); } else key_pressed['g'] = false;
			if (key[SDL_SCANCODE_D]) { cs = 'h'; print_char('h'); } else key_pressed['h'] = false;
			if (key[SDL_SCANCODE_SEMICOLON]) { cs = 'i'; print_char('i'); } else key_pressed['i'] = false;
			if (key[SDL_SCANCODE_Y]) { cs = 'j'; print_char('j'); } else key_pressed['j'] = false;
			if (key[SDL_SCANCODE_N]) { cs = 'k'; print_char('k'); } else key_pressed['k'] = false;
			if (key[SDL_SCANCODE_M]) { cs = 'l'; print_char('l'); } else key_pressed['l'] = false;
			if (key[SDL_SCANCODE_C]) { cs = 'm'; print_char('m'); } else key_pressed['m'] = false;
			if (key[SDL_SCANCODE_J]) { cs = 'n'; print_char('n'); } else key_pressed['n'] = false;
			if (key[SDL_SCANCODE_L]) { cs = 'o'; print_char('o'); } else key_pressed['o'] = false;
			if (key[SDL_SCANCODE_O]) { cs = 'p'; print_char('p'); } else key_pressed['p'] = false;
			if (key[SDL_SCANCODE_Q]) { cs = 'q'; print_char('q'); } else key_pressed['q'] = false;
			if (key[SDL_SCANCODE_E]) { cs = 'r'; print_char('r'); } else key_pressed['r'] = false;
			if (key[SDL_SCANCODE_S]) { cs = 's'; print_char('s'); } else key_pressed['s'] = false;
			if (key[SDL_SCANCODE_F]) { cs = 't'; print_char('t'); } else key_pressed['t'] = false; 
			if (key[SDL_SCANCODE_I]) { cs = 'u'; print_char('u'); } else key_pressed['u'] = false;
			if (key[SDL_SCANCODE_B]) { cs = 'v'; print_char('v'); } else key_pressed['v'] = false;
			if (key[SDL_SCANCODE_R]) { cs = 'w'; print_char('w'); } else key_pressed['w'] = false;
			if (key[SDL_SCANCODE_X]) { cs = 'x'; print_char('x'); } else key_pressed['x'] = false;
			if (key[SDL_SCANCODE_H]) { cs = 'y'; print_char('y'); } else key_pressed['y'] = false;
			if (key[SDL_SCANCODE_Z]) { cs = 'z'; print_char('z'); } else key_pressed['z'] = false;
			if (key[SDL_SCANCODE_PERIOD]) { cs = '.'; print_char('.'); } else key_pressed['.'] = false;
			if (key[SDL_SCANCODE_COMMA]) { cs = ','; print_char(','); } else key_pressed[','] = false;
			if (key[SDL_SCANCODE_MINUS]) { cs = '-'; print_char('-'); } else key_pressed['-'] = false;
			if (key[SDL_SCANCODE_RETURN]) { cs = '\n'; print_char('\n'); } else key_pressed['\n'] = false;			








*/



/*const nat width = (nat) surface->w;
			for (nat r = 0; r < lpixel_size; r++) {
				for (nat c = 0; c < lpixel_size; c++) {
					const nat at = width * (lpixel_size * row + r) + (lpixel_size * column + c);
					((uint32_t*) surface->pixels)[at] = (uint32_t) -1;
				}
			}
			if (column >= column_count) { 
				column = 0; 
				if (row >= row_count) row = 0; 
				else row++;
			} else column += 1;*/























































						//uint32_t color = -1;
						//if (fr < 9) color = (uint32_t) ((font[9 * character + fr] & (1 << fc)) ? -1 : 0);


				//printf("assert(%llu < 30);\n", character);


			//uint32_t color = -1;
						//if (fr < 9) color = (uint32_t) ((font[9 * character + fr] & (1 << fc)) ? -1 : 0);


			//uint32_t color = -1;
			//if (fr < 9) color = (uint32_t) ((font[9 * character + fr] & (1 << fc)) ? -1 : 0);


		// nanosleep((const struct timespec[]){{0, (1)}}, NULL);

















/*





				if (key[SDL_SCANCODE_E]) {
					column = 0; 
					if (row >= row_count) row = 0; 
					else row++;
				}

			

				if (key[SDL_SCANCODE_S]) { 

					SDL_LockSurface(surface);
					memset((uint32_t*) surface->pixels, 0, sizeof(uint32_t) * pixel_count);
					SDL_UnlockSurface(surface);

				}
				if (key[SDL_SCANCODE_A]) { 
					SDL_LockSurface(surface);

					
					const nat width = (nat) surface->w;
					for (nat r = 0; r < lpixel_size; r++) {
						for (nat c = 0; c < lpixel_size; c++) {
							const nat at = width * (lpixel_size * row + r) + (lpixel_size * column + c);
							((uint32_t*) surface->pixels)[at] = (uint32_t) -1;
						}
					}
					if (column >= column_count) { 
						column = 0; 
						if (row >= row_count) row = 0; 
						else row++;
					} else column += 1;
					SDL_UnlockSurface(surface);
				} 







	
		} else {
			if (SDL_WaitEvent(&event)) {
				const Uint8* key = SDL_GetKeyboardState(0);
				if (event.type == SDL_QUIT) { quit = true; }
				else if (event.type == SDL_WINDOWEVENT) {
					if (	event.window.event == SDL_WINDOWEVENT_RESIZED or 
						event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) resized = true;
				} else if (event.type == SDL_MOUSEMOTION) {
				} else if (event.type == SDL_KEYDOWN) {
					if (key[SDL_SCANCODE_Q]) quit = true; 
					if (key[SDL_SCANCODE_T]) task_running = not task_running;
					if (key[SDL_SCANCODE_A]) { 
						SDL_LockSurface(surface);
						const nat width = surface->w;
						for (nat r = 0; r < lpixel_size; r++) {
							for (nat c = 0; c < lpixel_size; c++) {
								const nat at = width * (lpixel_size * row + r) + (lpixel_size * column + c);
								((uint32_t*) surface->pixels)[at] = (uint32_t) -1;
							}
						}
						if (column >= column_count) { 
							column = 0; 
							if (row >= row_count) row = 0; 
							else row++;
						} else column++;
						SDL_UnlockSurface(surface);
					} 
				}
			}
			
		}		




*/










/*
0,0,0,0,0,0,0,0,0,0,

0,0,0,62,1,63,65,65,62

64,64,64,64,126,65,65,65,62

0,0,0,0,62,64,64,64,62

1,1,1,1,63,65,65,65,62

0,0,30,17,65,127,64,65,62

0,15,16,16,60,16,16,16,16

0,61,67,65,65,63,1,65,62,

64,64,64,126,65,65,65,65,65,

0,8,0,56,8,8,8,127,

0,8,0,24,8,8,8,8,112,

64,68,72,112,112,72,68,66,65,

56,8,8,8,8,8,8,8,7

0,0,0,42,93,73,73,65,65,

0,0,0,93,97,65,65,65,65,

0,0,0,0,62,65,65,65,62,

0,93,97,65,65,126,64,64,64,

0,29,67,65,65,63,1,1,1,

0,93,97,64,64,64,64,64,64,

0,0,62,65,64,62,1,65,62,

8,8,30,8,8,8,8,8,7,

0,0,0,65,65,65,65,67,29,

0,0,0,65,65,34,34,20,8,

0,0,0,65,65,73,73,93,42,

0,0,65,34,20,8,20,34,65,

  0,  0,  0, 65, 34, 20,  8, 16, 96,
  0,  0,127,  2,  4,  8, 16, 32,127,









          ^,.
2^6 = 64;      1000000
2^0 = 1;       0000001

....... 0
....... 0
....... 0
.#####. 62
......# 1
.###### 63
#.....# 65
#.....# 65
.#####. 62

0,0,0,62,1,63,65,65,62

#...... 64
#...... 64
#...... 64
#...... 64
######. 126
#.....# 65
#.....# 65
#.....# 65
.#####. 62

64,64,64,64,126,65,65,65,62

....... 0
....... 0
....... 0
....... 0
.#####. 62
#...... 64
#...... 64
#...... 64
.#####. 62

0,0,0,0,62,64,64,64,62

......# 1
......# 1
......# 1
......# 1
.###### 63
#.....# 65
#.....# 65
#.....# 65
.#####. 62

1,1,1,1,63,65,65,65,62

....... 0
....... 0
..####. 30
.#....# 17
#.....# 65
####### 127
#...... 64
#.....# 65
.#####. 62

0,0,30,17,65,127,64,65,62

....... 0
...#### 15
..#.... 16
..#.... 16
.####.. 60
..#.... 16
..#.... 16
..#.... 16
..#.... 16

0,15,16,16,60,16,16,16,16

....... 0
.####.# 61
#....## 67
#.....# 65
#.....# 65
.###### 63
......# 1
#.....# 65
.#####. 62

0,61,67,65,65,63,1,65,62,

#...... 64
#...... 64
#...... 64
######. 126
#.....# 65
#.....# 65
#.....# 65
#.....# 65
#.....# 65

64,64,64,126,65,65,65,65,65,

....... 0
...#... 8
....... 0
.###... 56
...#... 8
...#... 8
...#... 8
...#... 8
####### 127


0,8,0,56,8,8,8,127,

....... 0 
...#... 8
....... 0 
..##... 24
...#... 8
...#... 8
...#... 8
...#... 8
###.... 112

0,8,0,24,8,8,8,8,112,

#...... 64
#...#.. 68
#..#... 72
###.... 112
###.... 112
#..#... 72
#...#.. 68
#....#. 66
#.....# 65

64,68,72,112,112,72,68,66,65,

.###... 56
...#... 8
...#... 8
...#... 8
...#... 8
...#... 8
...#... 8
...#... 8
....### 7

56,8,8,8,8,8,8,8,7

....... 0 
....... 0 
....... 0 
.#.#.#. 42
#.###.# 93
#..#..# 73
#..#..# 73
#.....# 65
#.....# 65

0,0,0,42,93,73,73,65,65,

....... 0
....... 0
....... 0
#.####. 93
##....# 97
#.....# 65
#.....# 65
#.....# 65
#.....# 65

0,0,0,93,97,65,65,65,65,

....... 0
....... 0
....... 0
....... 0
.#####. 62
#.....# 65
#.....# 65
#.....# 65
.#####. 62

0,0,0,0,62,65,65,65,62,

....... 0
#.####. 93
##....# 97
#.....# 65
#.....# 65
######. 126
#...... 64
#...... 64
#...... 64

0,93,97,65,65,126,64,64,64,


....... 0
.####.# 29
#....## 67
#.....# 65
#.....# 65
.###### 63
......# 1
......# 1
......# 1

0,29,67,65,65,63,1,1,1,

....... 0
#.####. 94
##....# 97
#...... 64
#...... 64
#...... 64
#...... 64
#...... 64
#...... 64

0,93,97,64,64,64,64,64,64,

....... 0 
....... 0
.#####. 62
#.....# 65
#...... 64
.#####. 62
......# 1
#.....# 65
.#####. 62

0,0,62,65,64,62,1,65,62,

...#... 8
...#... 8
.#####. 30
...#... 8
...#... 8
...#... 8
...#... 8
...#... 8
....### 7

8,8,30,8,8,8,8,8,7,

....... 0
....... 0
....... 0
#.....# 65
#.....# 65
#.....# 65
#.....# 65
#....## 67
.####.# 29

0,0,0,65,65,65,65,67,29,

....... 0
....... 0
....... 0
#.....# 65
#.....# 65
.#...#. 34
.#...#. 34
..#.#.. 20
...#... 8


0,0,0,65,65,34,34,20,8,

....... 0
....... 0
....... 0 
#.....# 65
#.....# 65
#..#..# 73
#..#..# 73
#.###.# 93
.#.#.#. 42

0,0,0,65,65,73,73,93,93,42,

....... 0
....... 0
#.....# 65
.#...#. 34
..#.#.. 20
...#... 8
..#.#.. 20
.#...#. 34
#.....# 65

0,0,65,34,20,8,20,34,65,

....... 0
....... 0
....... 0
#.....# 65
.#...#. 34
..#.#.. 20
...#... 8
..#.... 16
##..... 96

0,0,0,65,34,20,8,16,96,

....... 0
....... 0
####### 127
.....#. 2
....#.. 4
...#... 8
..#.... 16
.#..... 32
####### 127

0,0,127,2,4,8,16,32,127


*/





//       a b c d e f g h i j k l m n o p q r s t u v w x y z   0 1 2 3 4 5 6 7 8 9  _ . , - 

//   7 x 9   bitmapped font   

// 			(10 x 8 pixels per char)

// bitmap font here












/*

static void print_char(char cs) {

	if (key_pressed[cs]) {
		key_pressed[cs]++;
		if (key_pressed[cs] < (cs == 127 ? 3 : 12)) goto skip_insert;
	} else key_pressed[cs] = 1;

	nat character = 1;
	if (cs >= 'a' and cs <= 'z') character = (nat) (cs - 'a') + 1;
	if (cs == ' ' or cs == 127) character = 0;
	if (cs == '.' ) character = 1 + 26 + 0; 
	if (cs == ',' ) character = 1 + 26 + 1; 
	if (cs == '-' ) character = 1 + 26 + 2;
	if (cs == '\n') character = 1 + 26 + 3;

	nat rowi = 0, columni = 0;
	for (nat fr = 0; fr < 10; fr++) {
		for (nat fc = 8; fc--; ) {
			const nat width = (nat) surface->w;
			for (nat r = 0; r < lpixel_size; r++) {
				for (nat c = 0; c < lpixel_size; c++) {
					const nat at = width * (lpixel_size * (row + rowi) + r) + (lpixel_size * (column + columni) + c);
					((uint32_t*) surface->pixels)[at] = (uint32_t) 0;
				}
			}
			columni++;
		}
		columni = 0; rowi++;
	}
	if (cs == 127) {
		if (column < 8) { 
			column = column_count - 1; 
			if (row >= 10) row -= 10;
		} else column -= 8;
	}
	SDL_LockSurface(surface);

	rowi = 0; columni = 0;
	for (nat fr = 0; fr < 10; fr++) {
		for (nat fc = 8; fc--; ) {
			uint32_t color = 0;
			if (fr < 9) color = (uint32_t) ((font[9 * character + fr] & (1 << fc)) ? -1 : 0);
			const nat width = (nat) surface->w;
			for (nat r = 0; r < lpixel_size; r++) {
				for (nat c = 0; c < lpixel_size; c++) {
					const nat at = width * (lpixel_size * (row + rowi) + r) + (lpixel_size * (column + columni) + c);
					((uint32_t*) surface->pixels)[at] = (uint32_t) color;
				}
			}
			columni++;
		}
		columni = 0; rowi++;
	}
	if (cs == '\n') {
		column = 0;
		if (row >= row_count - 1) row = 0; else row += 10;
	} else if (cs != 127) {
		if (column >= column_count - 1) { 
			column = 0; 
			if (row >= row_count - 1) row = 0; else row += 10;
		} else column += 8;
	}

	rowi = 0; columni = 0;
	for (nat fr = 0; fr < 10; fr++) {
		for (nat fc = 8; fc--; ) {
			const nat width = (nat) surface->w;
			for (nat r = 0; r < lpixel_size; r++) {
				for (nat c = 0; c < lpixel_size; c++) {
					const nat at = width * (lpixel_size * (row + rowi) + r) + (lpixel_size * (column + columni) + c);
					((uint32_t*) surface->pixels)[at] = (uint32_t) -1;
				}
			}
			columni++;
		}
		columni = 0; rowi++;
	}
	SDL_UnlockSurface(surface);
	skip_insert:;
	SDL_ShowCursor(SDL_DISABLE);
}





















			if (key[SDL_SCANCODE_T]) { cs = 'b'; print_char('b'); } else key_pressed['b'] = false;
			if (key[SDL_SCANCODE_V]) { cs = 'c'; print_char('c'); } else key_pressed['c'] = false;
			if (key[SDL_SCANCODE_W]) { cs = 'd'; print_char('d'); } else key_pressed['d'] = false;
			if (key[SDL_SCANCODE_K]) { cs = 'e'; print_char('e'); } else key_pressed['e'] = false;
			if (key[SDL_SCANCODE_U]) { cs = 'f'; print_char('f'); } else key_pressed['f'] = false;
			if (key[SDL_SCANCODE_G]) { cs = 'g'; print_char('g'); } else key_pressed['g'] = false;
			if (key[SDL_SCANCODE_D]) { cs = 'h'; print_char('h'); } else key_pressed['h'] = false;
			if (key[SDL_SCANCODE_SEMICOLON]) { cs = 'i'; print_char('i'); } else key_pressed['i'] = false;
			if (key[SDL_SCANCODE_Y]) { cs = 'j'; print_char('j'); } else key_pressed['j'] = false;
			if (key[SDL_SCANCODE_N]) { cs = 'k'; print_char('k'); } else key_pressed['k'] = false;
			if (key[SDL_SCANCODE_M]) { cs = 'l'; print_char('l'); } else key_pressed['l'] = false;
			if (key[SDL_SCANCODE_C]) { cs = 'm'; print_char('m'); } else key_pressed['m'] = false;
			if (key[SDL_SCANCODE_J]) { cs = 'n'; print_char('n'); } else key_pressed['n'] = false;
			if (key[SDL_SCANCODE_L]) { cs = 'o'; print_char('o'); } else key_pressed['o'] = false;
			if (key[SDL_SCANCODE_O]) { cs = 'p'; print_char('p'); } else key_pressed['p'] = false;
			if (key[SDL_SCANCODE_Q]) { cs = 'q'; print_char('q'); } else key_pressed['q'] = false;
			if (key[SDL_SCANCODE_E]) { cs = 'r'; print_char('r'); } else key_pressed['r'] = false;
			if (key[SDL_SCANCODE_S]) { cs = 's'; print_char('s'); } else key_pressed['s'] = false;
			if (key[SDL_SCANCODE_F]) { cs = 't'; print_char('t'); } else key_pressed['t'] = false; 
			if (key[SDL_SCANCODE_I]) { cs = 'u'; print_char('u'); } else key_pressed['u'] = false;
			if (key[SDL_SCANCODE_B]) { cs = 'v'; print_char('v'); } else key_pressed['v'] = false;
			if (key[SDL_SCANCODE_R]) { cs = 'w'; print_char('w'); } else key_pressed['w'] = false;
			if (key[SDL_SCANCODE_X]) { cs = 'x'; print_char('x'); } else key_pressed['x'] = false;
			if (key[SDL_SCANCODE_H]) { cs = 'y'; print_char('y'); } else key_pressed['y'] = false;
			if (key[SDL_SCANCODE_Z]) { cs = 'z'; print_char('z'); } else key_pressed['z'] = false;
			if (key[SDL_SCANCODE_PERIOD]) { cs = '.'; print_char('.'); } else key_pressed['.'] = false;
			if (key[SDL_SCANCODE_COMMA]) { cs = ','; print_char(','); } else key_pressed[','] = false;
			if (key[SDL_SCANCODE_MINUS]) { cs = '-'; print_char('-'); } else key_pressed['-'] = false;
			if (key[SDL_SCANCODE_RETURN]) { cs = '\n'; print_char('\n'); } else key_pressed['\n'] = false;			

















*/








