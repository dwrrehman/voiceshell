// minimal cocoa example application in objc.

// compile me with:
//    clang -Weverything -framework Cocoa main.m -o run

#import <Cocoa/Cocoa.h>

int main(void) {
    [NSAutoreleasePool new];
    [NSApplication sharedApplication];
    [NSApp setActivationPolicy:NSApplicationActivationPolicyRegular];
    dispatch_async(dispatch_get_main_queue(), ^{
        [NSApp activateIgnoringOtherApps:YES];
    });

    id bundleInfo = [[NSBundle mainBundle] infoDictionary];
    id appName = [bundleInfo objectForKey:@"CFBundleName"];
    id appVersion = [bundleInfo objectForKey:@"CFBundleVersion"];

    id quitMenuItem = [[NSMenuItem alloc] autorelease];
    [quitMenuItem
        initWithTitle:@"Quit editor"
        action:@selector(terminate:)
        keyEquivalent:@"q"];

    id appMenu = [[NSMenu new] autorelease];
    [appMenu addItem:quitMenuItem];
    id appMenuItem = [[NSMenuItem new] autorelease];
    [appMenuItem setSubmenu:appMenu];

    id mainMenu = [[NSMenu new] autorelease];
    [mainMenu addItem:appMenuItem];
    [NSApp setMainMenu:mainMenu];

    id window = [[NSWindow alloc] autorelease];
    [window
        initWithContentRect:NSMakeRect(0, 0, 320, 240)
        styleMask:NSTitledWindowMask
        backing:NSBackingStoreBuffered
        defer:NO];

    [window setTitle:[[appName stringByAppendingString:@" v"] stringByAppendingString:appVersion]];
    [window center];
    [window makeKeyAndOrderFront:nil];
    [NSApp run];
}


