// based on https://stackoverflow.com/a/59596600/834108
// Minimal Pure C code to create a window in Cocoa
// Adapted to work on ARM64

// compile me with:     clang minimal.c -framework Cocoa -o minimal.app

#include <objc/runtime.h>
#include <objc/message.h>
#include <Carbon/Carbon.h>

#define cls objc_getClass
#define sel sel_getUid
#define msg ((id (*)(id, SEL))objc_msgSend)
#define msg_int ((id (*)(id, SEL, int))objc_msgSend)
#define msg_id  ((id (*)(id, SEL, id))objc_msgSend)
#define msg_ptr ((id (*)(id, SEL, void*))objc_msgSend)
#define msg_cls ((id (*)(Class, SEL))objc_msgSend)
#define msg_cls_chr ((id (*)(Class, SEL, char*))objc_msgSend)

enum NSApplicationActivationPolicy {
    NSApplicationActivationPolicyRegular   = 0,
    NSApplicationActivationPolicyAccessory = 1,
    NSApplicationActivationPolicyERROR     = 2,
};

enum NSWindowStyleMask {
    NSWindowStyleMaskBorderless     = 0,
    NSWindowStyleMaskTitled         = 1 << 0,
    NSWindowStyleMaskClosable       = 1 << 1,
    NSWindowStyleMaskMiniaturizable = 1 << 2,
    NSWindowStyleMaskResizable      = 1 << 3,
};

typedef enum NSBackingStoreType {
    NSBackingStoreBuffered = 2,
} NSBackingStoreType;

int main(void) { //int argc, const char** argv

	id app = msg_cls(cls("NSApplication"), sel("sharedApplication"));
	msg_int(app, sel("setActivationPolicy:"), NSApplicationActivationPolicyRegular);




	puts("setting up the menu...");





	struct CGRect frameRect = {{0, 0}, {1000, 900}};
	id window = ((id (*)(id, SEL, struct CGRect, int, int, int))objc_msgSend)(
		msg_cls(cls("NSWindow"), sel("alloc")),
		sel("initWithContentRect:styleMask:backing:defer:"),
		frameRect,
		NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | 
		NSWindowStyleMaskResizable | NSWindowStyleMaskMiniaturizable,
		NSBackingStoreBuffered,
		false
	);
	msg_id(window, sel("setTitle:"), msg_cls_chr(cls("NSString"), sel("stringWithUTF8String:"), "editor"));
	msg_ptr(window, sel("makeKeyAndOrderFront:"), nil);
	msg_int(app, sel("activateIgnoringOtherApps:"), true);
	msg(app, sel("run"));
}


















/*


	id menubar = [[NSMenu new] autorelease];
	id appMenuItem = [[NSMenuItem new] autorelease];
	[menubar addItem:appMenuItem];
	[NSApp setMainMenu:menubar];
	id appMenu = [[NSMenu new] autorelease];
	id appName = [[NSProcessInfo processInfo] processName];
	id quitTitle = [@"Quit " stringByAppendingString:appName];
	id quitMenuItem = [[[NSMenuItem alloc] initWithTitle:quitTitle
	action:@selector(terminate:) keyEquivalent:@"q"] autorelease];
	[appMenu addItem:quitMenuItem];
	[appMenuItem setSubmenu:appMenu];



*/



