// main.cpp
#include <stdlib.h>
#include <stdio.h>

#include <CoreAudio/CoreAudio.h>
#include <CoreServices/CoreServices.h>

int
main (void)
{
    AudioObjectPropertyAddress property = {
        kAudioHardwarePropertyDevices,
        kAudioObjectPropertyScopeGlobal,
        kAudioObjectPropertyElementMaster
    };

    OSStatus status;
    UInt32 size;

    status = AudioObjectGetPropertyDataSize(
            kAudioObjectSystemObject,
            &property,
            0,
            NULL,
            &size);

    printf("status(%d), size: %d\n", status, size);

    return EXIT_SUCCESS;
}
