// core audio example in c
// possible replacement for our sdl audio backend. 
// written on 1202412172.005102 dwrr

// copied from online


#include <CoreAudio/CoreAudio.h>
#include <stdio.h>

int main(void) {
    AudioDeviceID defaultOutputDevice;
    UInt32 size = sizeof(defaultOutputDevice);
    AudioObjectPropertyAddress propertyAddress = {
        kAudioHardwarePropertyDefaultOutputDevice,
        kAudioObjectPropertyScopeGlobal,
        kAudioObjectPropertyElementMain
    };
    OSStatus status = AudioObjectGetPropertyData(
        kAudioObjectSystemObject,
        &propertyAddress,
        0,
        NULL,
        &size,
        &defaultOutputDevice
    );
    if (status != noErr) {
        printf("Error getting default output device: %d\n", (int)status);
        return 1;
    }
    printf("Default output device ID: %d\n", (int)defaultOutputDevice);
}














































/*
#include <CoreAudio/CoreAudio.h>
#include <AudioToolbox/AudioToolbox.h>

int main(int argc, char *argv[]) {
    AudioComponentDescription desc;
    desc.componentType = kAudioUnitType_Output;
    desc.componentSubType = kAudioUnitSubType_DefaultOutput;
    desc.componentManufacturer = kAudioUnitManufacturer_Apple;
    desc.componentFlags = 0;
    desc.componentFlagsMask = 0;

    AudioComponent comp = AudioComponentFindNext(NULL, &desc);
    if (comp == NULL) {
        fprintf(stderr, "Error finding default output unit\n");
        return 1;
    }

    AudioUnit outputUnit;
    AudioUnitInitialize(outputUnit);

    // Start the audio unit
    AudioOutputUnitStart(outputUnit);

    // Sleep for 5 seconds to hear the sound
    sleep(5);

    // Stop the audio unit
    AudioOutputUnitStop(outputUnit);

    AudioUnitUninitialize(outputUnit);

    return 0;
}


*/



