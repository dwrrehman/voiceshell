// this program is to allow me to control my computr  rer only using my voice, and sound.
// ie, output and input are voice- using talon for inputting words,
// and "say" command for outtputing words. 
// writing this using my own text editor, because,
// i am writing it because i am on  noin pain and
//   can't move my hands  / arms that much, so yeah.

////        the "voice shell"    aka   the "conversation" utility. 

// a utility to allow me to control my computer, and use the shell and stuff, only using my voice.
// and have the computer respond back to me, using its voice. (ie, text to speech, using "say").

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdnoreturn.h> 
// aoshten
#include <ctype.h>
#include <time.h>
#include <string.h> 
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <iso646.h>
#include <fcntl.h>
#include <termios.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/wait.h>

// static const char* autosave_directory = "/Users/dwrr/Documents/personal/autosaves/";
// static const nat autosave_frequency = 100;

/*
	features/functions to implment:

	CONFIG
		- set page size
		- set dipsplay entry size
		- set code name

	JOBS
		- start job
		- halt job
		- delete halted job
		- pause job
		- resume job
	x	- list jobs and count
	x	- new buffer job
	x	- switch job XXX
	x	- display current status

	ARGUMENTS
		- push new argument
		- delete last argument
		- empty argument list
		- show .this argument list

	INPUT/OUTPUT
		- get stdout entry
		- submit stdin entry

	TEXT EDITOR
		- go into insert mode
		- move right / left / wordleft / wordright / etc....
		- write buffer to file 
		- load file into buffer
		- show filename
		- display buffer

	PAGING
	x	- go to next page      ie increment page pointer
	x	- go to previous page  ie decrement page pointer
	x	- repeat
	x	- go to last page
	x	- go to first page
	x	- go to previous entry
	x	- go to next entry

	REMAP
		- load remappings file
		- clear remappings
		- set new remap entry key
		- submit remap entry value
		- list current remappings




	new design for the text editing capability in the paging system:

		- we will actually make each page actually editable! 
	
		- pages will have a cursor, 
		
		- pages will still be completely word based, character mode only makes 

		- - oh crap actually, we need to make the original text there, so that we can edit that text, 

			- and then we will change the raw text,   and the regenerate the character, and 

		- oh wait, how do we move to another page?

			i think we actually move to the next page naturally, then. or like, when we are inserting text, it automatically puts us into a new page, when we insert enough words into it. 

				but then this also means that we need to be actively re-calibrating the pages while the used is inserting text, characetr by chacter, or word by word. 



		

				orrrrr maybe we just dynamically move our pointer, and reindex the entire output entry,  if we are editing the document!


						ie, we just re-page the whole entry, on each edit,     (insert/delete)


					and the user makes their changes to the raw text,    that is associated with the output entry!!! thats the right way to do this.   nice.  okay cool. 



	- so basically, we just need to store the real text for a given output entry, along side the set of pages.
		and then edit that text,    if its editable output. so yeah. 

			ie, the text editor     isssssss    the last output entry, which just happens to be editable.





	






*/

static const char punctuation_characters[] = " \t\n~:;.,-/*=+^%$#@&|!?\\_()[]{}<>\"`";
static const int phrase_delay_x100ms = 3;

static const char* digit_spelling[] = {
	"zero", "one", "two", "three", "four", 
	"five", "six", "seven", "eight", "nine"
};

static const char* letter_spelling[] = {
	"–– A––",  "–– bee––",   "–– see––",   "–– dee––", 
	"–– E––", "–– ehf––",  "–– gee––",  "–– H––",
	"–– eye––",  "–– jay––",  "–– kay", "–– ell––",
	"–– em––", "–– en––",  "–– oh––",   "–– pee––",
	"–– queue––", "–– are––", "–– ess––",   "–– tee––",
	"–– you––", "–– vee––",  "–– double you––", "–– ex––",
	"–– why––", "–– zee––"
};

static const char* capital_letter_spelling[] = {
	"–– large air––", "–– large bat––", "–– large cap––", "–– large drum––", 
	"–– large each––", "–– large fine––", "–– large gust––", "–– large harp––",
	"–– large sit––", "–– large jury––", "–– large crunch––", "–– large look––",
	"–– large made––", "–– large near––", "–– large odd––", "–– large pit––",
	"–– large quench––", "–– large red––", "–– large sun––", "–– large trap––",
	"–– large urge––", "–– large vest––", "–– large whale––", "–– large plex––",
	"–– large yank––", "–– large zip––"
};

static const char* default_codenames[] = {
	"green elephant", 
	"blue octopus", 
	"red monkey",
};

typedef uint64_t nat;

enum output_entry_types { 
	type_output, 
	type_draft_input, 
	type_input 
};

struct action {
	nat parent;
	nat pre;
	nat post;
	uint32_t choice;
	bool inserting;
	char c;
	uint16_t _padding;
};

struct buffer {
	char* text;
	char* clipboard;
	struct action* actions;
	char* filename;
	char* autosavename;
	nat cursor;
	nat count; 
	nat anchor;
	nat head;
	nat actioncount;
       	nat cliplength;
	nat autosavecounter;
	nat selecting;
};

struct page {
	char** characters;
	nat charactercount;
	char** words;
	nat wordcount;
};

struct entry {
	struct buffer data;
	struct page* pages;
	nat pagecount;
	nat pagepointer;
	nat type;
};

struct job {
	struct entry* entries;
	char* codename;
	char** args;
	nat argcount;
	nat entrycount;
	nat entrypointer;
	int status;
	int output_mode;
};


static bool running = 0;
static nat pagesize = 10;
static nat outputentrysize = 100;

static struct job* jobs = 0;
static nat jobcount = 0;
static nat jobpointer = 0;

static char* history[64] = {0};
static char spelling_string[4096] = {0};

static char** dictionary = NULL;
static nat dictionary_count = 0;

static struct termios terminal = {0};
extern char** environ;

static void trim_whitespace(char** string, nat* length) {
	while (**string) {
		if (not isspace(**string)) break; 
		(*string)++; (*length)--;
	}
	while (*length) {
		if (not isspace((*string)[*length - 1])) break;
		(*length)--;
	}
}

static void lowercase_string(char* string, nat length) {
	for (nat i = 0; i < length; i++) {
		string[i] = (char) tolower(string[i]);
	}
}

static void create_process(char** args) {
	pid_t pid = fork();
	if (pid < 0) { perror("fork"); getchar(); return; }
	if (not pid) {
		if (execve(args[0], args, environ) < 0) { perror("execve"); exit(1); }
	} 
	int status = 0;
	if ((pid = wait(&status)) == -1) { perror("wait"); getchar(); return; }
	char dt[32] = {0};
	struct timeval t = {0};
	gettimeofday(&t, NULL);
	struct tm* tm = localtime(&t.tv_sec);
	strftime(dt, 32, "1%Y%m%d%u.%H%M%S", tm);
	if (WIFEXITED(status)) 		{}//printf("[%s:(%d) exited with code %d]\n", dt, pid, WEXITSTATUS(status));
	else if (WIFSIGNALED(status)) 	{}//printf("[%s:(%d) was terminated by signal %s]\n", dt, pid, strsignal(WTERMSIG(status)));
	else if (WIFSTOPPED(status)) 	{}//printf("[%s:(%d) was stopped by signal %s]\n", 	dt, pid, strsignal(WSTOPSIG(status)));
	else 				{}//printf("[%s:(%d) terminated for an unknown reason]\n", dt, pid);
	//fflush(stdout);
	// return status;
}

static void raw_say(const char* text) { create_process((char*[]) {strdup("/usr/bin/say"), strdup(text), 0}); }

static void input_off(void) { 
	create_process((char*[]) {
		strdup("/bin/bash"), 
		strdup("-c"), 
		strdup("echo 'actions.sound.set_microphone(\"None\")' | ~/.talon/bin/repl > /dev/null"), 
		0
	});
}

static void input_on(void) { 
	create_process((char*[]) {
		strdup("/bin/bash"), 
		strdup("-c"), 
		strdup("echo 'actions.sound.set_microphone(\"System Default\")' | ~/.talon/bin/repl > /dev/null"), 
		0
	});
}

static noreturn void interrupted(int _) {if(_){} tcsetattr(0, TCSAFLUSH, &terminal); input_off(); exit(0);  }

static void say(const char* string) {
	input_off();
	raw_say(string);
	printf("--> %s\n", string);
	input_on();
}

static bool they_said(const char* literal) {
	char* words[64] = {0};
	nat word_count = 0;
	const nat length = (nat) strlen(literal);
	nat start = 0, count = 0;
	for (nat index = 0; index < length; index++) {
		if (not isspace(literal[index])) { 
			if (not count) start = index;
			count++; continue;
		} else if (not count) continue;
		process:;
			const char* word = literal + start;
			words[word_count++] = strndup(word, count);
			count = 0;
	}
	if (count) goto process;
	for (nat i = word_count, h = 0; i--; h++) {
		if (not history[h] or strcmp(words[i], history[h])) return false;
		free(words[i]);
	}
	return true;
}

static char* generate_codename(nat i) {
	if (i >= sizeof default_codenames / sizeof *default_codenames) return strdup("maximum default name");
	return strdup(default_codenames[i]);
}

static const char* spell_string_number(const char* digits, nat digit_count) {
	strlcpy(spelling_string, "number–– ", sizeof spelling_string);
	for (nat i = 0; i < digit_count; i++) {
		strlcat(spelling_string, digit_spelling[(digits[i] - '0') % 10], sizeof spelling_string);
		strlcat(spelling_string, " ", sizeof spelling_string);
	}
	strlcat(spelling_string, "––", sizeof spelling_string);
	return spelling_string;
}

static const char* spell_number(nat number) {
	char digits[128] = {0};
	const nat digit_count = (nat) snprintf(digits, sizeof digits, "%llu", number);
	return spell_string_number(digits, digit_count);
}

static const char* spell_character(char c) { 
	if (c >= '0' and c <= '9') return digit_spelling[(c - '0') % 10];
	if (c >= 'A' and c <= 'Z') return capital_letter_spelling[(c - 'A') % 26];
	if (c >= 'a' and c <= 'z') return letter_spelling[(c - 'a') % 26];
	if (c == '\t') return "–– tab––";
	if (c == '\n') return "–– break––";
	if (c == '~') return "–– tilde––";
	if (c == ':') return "–– colon––";
	if (c == ';') return "–– semicolon––";
	if (c == '.') return "–– period––";
	if (c == ',') return "–– comma––";
	if (c == '-') return "–– hyphen––";
	if (c == '/') return "–– slash––";
	if (c == '*') return "–– asterisk––";
	if (c == '=') return "–– equal sign––";
	if (c == '+') return "–– plus sign––";
	if (c == '^') return "–– caret sign––";
	if (c == '%') return "–– percent sign––";
	if (c == '$') return "–– dollar sign––";
	if (c == '#') return "–– octothorpe––";
	if (c == '@') return "–– anthropod––";
	if (c == '&') return "–– ampersand––";
	if (c == '|') return "–– vertical––";
	if (c == '!') return "–– exclamation point––";
	if (c == '?') return "–– question mark––";
	if (c =='\\') return "–– back slash––";
	if (c == '_') return "–– underscore––";
	if (c == '(') return "–– open round––";
	if (c == ')') return "–– close round––";
	if (c == '[') return "–– open box––";
	if (c == ']') return "–– close box––";
	if (c == '{') return "–– open curl––";
	if (c == '}') return "–– close curl––";
	if (c == '<') return "–– open angle––";
	if (c == '>') return "–– close angle––";
	if (c == '"') return "–– double quote––";
	if (c == '\'')return "–– apostrophe––";
	if (c == '`') return "–– half quote––";

	memset(spelling_string, 0, sizeof spelling_string);
	snprintf(spelling_string, sizeof spelling_string, "unknown character %llu", (nat) c);
	return spelling_string;
}

static bool is_punctuation(char c) {
	for (nat i = 0; i < sizeof punctuation_characters; i++) {
		if (c == punctuation_characters[i]) return true;
	}
	return false;
}

static bool is_english_word(const char* word) {
	for (nat i = 0; i < dictionary_count; i++) {
		if (not strcmp(word, dictionary[i])) {
			return true;
		}
	}
	return false;
}

static bool is_all_digits(const char* word, nat count) {
	for (nat i = 0; i < count; i++) {
		if (not isdigit(word[i])) return false;
	}
	return true;
}

static char* pronounciation(char* word) {
	if (not strcmp("usable", word)) { free(word); return strdup("yoozable"); }
	else return word;
}

static void output_information(nat given_type, const char* string) {

	//puts("outputting: ");
	//puts(string);

	const nat length = strlen(string);
	char** words = NULL;
	nat word_count = 0;
	nat start = 0, count = 0;
	
	for (nat index = 0; index < length; index++) {

		//printf("looking at c=\"%c\"...\n", string[index]);

		if (not is_punctuation(string[index])) {
			if (not count) start = index;
			count++; continue;
		} else if (not count) goto next;
	process:;
		char* word = strndup(string + start, count);

		if (is_english_word(word)) {
			words = realloc(words, sizeof(char*) * (word_count + 1));
			words[word_count++] = pronounciation(word);
			goto next; 

		} else if (is_all_digits(word, count)) {
			words = realloc(words, sizeof(char*) * (word_count + 1));
			words[word_count++] = strdup(spell_string_number(word, count));
			free(word);
			goto next;

		} else if (count == 1) {
			words = realloc(words, sizeof(char*) * (word_count + 1));
			words[word_count++] = strdup(spell_character(*word));
			free(word);
			goto next;
		} else {
			for (nat i = 0; i < count; i++) {
				words = realloc(words, sizeof(char*) * (word_count + 1));
				words[word_count++] = strdup(spell_character(word[i]));
			}
			free(word);
			goto next;
		}

	next:	count = 0;
		if (string[index] != 32 and string[index]) { 
			words = realloc(words, sizeof(char*) * (word_count + 1));
			words[word_count++] = strdup(spell_character(string[index]));
		}
	}
	if (count) goto process;

	struct entry entry = { .type = given_type };
	struct page* pages = NULL;
	struct page page = {0};
	nat page_count = 0;

	for (nat i = 0; i < word_count; i++) {
		if (page.wordcount >= pagesize) {
			pages = realloc(pages, sizeof(struct page) * (page_count + 1));
			pages[page_count++] = page;
			page.wordcount = 0;
			page.words = NULL;
		}
		page.words = realloc(page.words, sizeof(char*) * (page.wordcount + 1));
		page.words[page.wordcount++] = words[i];
	}

	if (page.wordcount) {
		pages = realloc(pages, sizeof(struct page) * (page_count + 1));
		pages[page_count++] = page;
	}

	entry.data = (struct buffer) {
		.text = strdup(string),
		.count = strlen(string),
	};

	entry.pages = pages;
	entry.pagecount = page_count;

	jobs[jobpointer].entries = realloc(jobs[jobpointer].entries, sizeof(struct entry) * (jobs[jobpointer].entrycount + 1));
	jobs[jobpointer].entries[jobs[jobpointer].entrycount++] = entry;
}

static const char* help_string = 
	"switch to box code name here\n"
	"list all boxes\n"
	"create new box\n"
	"where am i\n"
	"get the current status\n"
	"get the current page size\n"
	"output some example information\n"
	"go to previous box\n"
	"go to next box\n"
	"go to previous entry\n"
	"go to next entry\n"
	"go to first page\n"
	"go to last page\n"
	"read page\n"
	"previous page\n"
	"next page\n"
	"repeat that again\n"
	"help me\n"
;

static void process_word(char* word) {

	if (not strcmp(word, "they're")) { free(word); word = strdup("there"); } 
	if (not strcmp(word, "bonks")) { free(word); word = strdup("box"); } 
	if (not strcmp(word, "vox")) { free(word); word = strdup("box"); } 
	if (not strcmp(word, "crank")) { free(word); word = strdup("current"); } 
	if (not strcmp(word, "crap")) { free(word); word = strdup("current"); } 
	if (not strcmp(word, "hope")) { free(word); word = strdup("help"); } 
	if (not strcmp(word, "al")) { free(word); word = strdup("all"); } 
	if (not strcmp(word, "tall")) { free(word); word = strdup("all"); } 
	if (not strcmp(word, "pitch")) { free(word); word = strdup("page"); } 
	if (not strcmp(word, "corn")) { free(word); word = strdup("current"); } 
	if (not strcmp(word, "cramp")) { free(word); word = strdup("current"); } 
	if (not strcmp(word, "cur")) { free(word); word = strdup("current"); } 

	free(history[63]);
	memmove(history + 1, history, (64 - 1) * sizeof(char*));
	*history = word;

	//printf("process_word: processing \"%s\"\n", word);
	
	if (they_said("please close the application now"))	{ say("terminating now"); running = false; }

	else if (they_said("i am clueless")) 			say("maybe try saying hello there from space");
	else if (they_said("hello there from space")) 		say("hello daniel, how are you today?");
	else if (they_said("i'm doing good")) 			say("well that's nice to hear");
	else if (they_said("how are you"))			say("i'm doing well, thanks for asking.");

	else if (they_said("help me")) { 
		if (not jobcount) { say("there are no boxes right now, so your question is not sensical"); return; } 

		output_information(type_output, help_string); 
		say("say read page and next page and previous page to browse the help menu"); 
	}

	else if (they_said("output some example information")) {
		if (not jobcount) { say("there are no boxes right now, so your question is not sensical"); return; } 

		say("generating some example output in standard output");
		output_information(type_output, "entry_type_output : yeah, lol, i think its good enough");


	} else if (they_said("output the alphabet")) {
		if (not jobcount) { say("there are no boxes right now, so your question is not sensical"); return; } 

		say("generating some example text containing the alphabet");
		output_information(type_output, "a b c d e f g h i j k l m n o p q r s t u v w x y z. "
						"...and also heres the numbers, 0 1 2 3 4 5 6 7 8 9"
		);


	} else if (they_said("repeat that again") or they_said("read page")) {
	read_current_page:
		if (not jobcount) { say("there are no boxes right now"); return; } 
		const struct job job = jobs[jobpointer];

		if (not job.entrycount) { say("there are no output entries right now"); return; } 
		const struct entry entry = job.entries[job.entrypointer];

		if (not entry.pagecount) { say("there are no pages right now"); return; } 
		const struct page page = entry.pages[entry.pagepointer];

		char string[128] = {0};
		snprintf(string, sizeof string, "on %llu of %llu––", entry.pagepointer, entry.pagecount);
		say(string);

		char* full_string = NULL;
		nat string_length = 0;

		for (nat i = 0; i < page.wordcount; i++) {
			const nat n = (nat) strlen(page.words[i]);
			full_string = realloc(full_string, string_length + n + 1);
			memcpy(full_string + string_length, page.words[i], n);
			string_length += n;
			full_string[string_length++] = ' ';
		}
		full_string = realloc(full_string, string_length + 1);
		full_string[string_length++] = 0;
		say(full_string);
		say("–– end of page");


	} else if (they_said("go to next entry")) { 
		if (not jobcount) { say("there are no boxes at the moment"); return; } 
		if (not jobs[jobpointer].entrycount) { say("there are no output entries right now"); return; } 
		if (jobs[jobpointer].entrypointer >= jobs[jobpointer].entrycount - 1) { say("already at last output entry"); return; } 

		jobs[jobpointer].entrypointer++; 
		char string[128] = {0};
		snprintf(string, sizeof string, "now looking at entry %llu of %llu––", jobs[jobpointer].entrypointer, jobs[jobpointer].entrycount);
		say(string);


	} else if (they_said("go to previous entry")) { 
		if (not jobcount) { say("there are no boxes at the moment"); return; } 
		if (not jobs[jobpointer].entrycount) { say("there are no output entries right now"); return; } 
		if (not jobs[jobpointer].entrypointer) { say("already at first output entry"); return; } 

		jobs[jobpointer].entrypointer--; 
		char string[128] = {0};
		snprintf(string, sizeof string, "now looking at entry %llu of %llu––", jobs[jobpointer].entrypointer, jobs[jobpointer].entrycount);
		say(string);


	} else if (they_said("next page")) { 
		if (not jobcount) { say("there are no boxes at the moment"); return; } 
		if (not jobs[jobpointer].entrycount) { say("there are no output entries right now"); return; } 
		if (jobs[jobpointer].entries[jobs[jobpointer].entrypointer].pagepointer 
		 >= jobs[jobpointer].entries[jobs[jobpointer].entrypointer].pagecount - 1) { say("already at last page"); return; } 

		jobs[jobpointer].entries[jobs[jobpointer].entrypointer].pagepointer++; 
		//char string[128] = {0};
		//snprintf(string, sizeof string, "now looking at page %llu of %llu––", 
		//	jobs[jobpointer].entries[jobs[jobpointer].entrypointer].pagepointer, 
		//	jobs[jobpointer].entries[jobs[jobpointer].entrypointer].pagecount
		//);
		//say(string);

		goto read_current_page;


	} else if (they_said("previous page")) { 
		if (not jobcount) { say("there are no boxes at the moment"); return; } 
		if (not jobs[jobpointer].entrycount) { say("there are no output entries right now"); return; } 
		if (not jobs[jobpointer].entries[jobs[jobpointer].entrypointer].pagepointer) { say("already at first page"); return; } 

		jobs[jobpointer].entries[jobs[jobpointer].entrypointer].pagepointer--; 
		//char string[128] = {0};
		//snprintf(string, sizeof string, "now looking at page %llu of %llu––", 
		//	jobs[jobpointer].entries[jobs[jobpointer].entrypointer].pagepointer, 
		//	jobs[jobpointer].entries[jobs[jobpointer].entrypointer].pagecount
		//);
		//say(string);

		goto read_current_page;


	} else if (they_said("go to first page")) { 
		if (not jobcount) { say("there are no boxes at the moment"); return; } 
		if (not jobs[jobpointer].entrycount) { say("there are no output entries right now"); return; } 
		if (not jobs[jobpointer].entries[jobs[jobpointer].entrypointer].pagepointer) { say("already at first page"); return; } 

		jobs[jobpointer].entries[jobs[jobpointer].entrypointer].pagepointer = 0; 
		char string[128] = {0};
		snprintf(string, sizeof string, "now looking at page %llu of %llu––", 
			jobs[jobpointer].entries[jobs[jobpointer].entrypointer].pagepointer, 
			jobs[jobpointer].entries[jobs[jobpointer].entrypointer].pagecount
		);
		say(string);


	} else if (they_said("go to last page")) { 
		if (not jobcount) { say("there are no boxes at the moment"); return; } 
		if (not jobs[jobpointer].entrycount) { say("there are no output entries right now"); return; } 
		if (jobs[jobpointer].entries[jobs[jobpointer].entrypointer].pagepointer 
		 >= jobs[jobpointer].entries[jobs[jobpointer].entrypointer].pagecount - 1) { say("already at last page"); return; } 

		jobs[jobpointer].entries[jobs[jobpointer].entrypointer].pagepointer 
		= jobs[jobpointer].entries[jobs[jobpointer].entrypointer].pagecount - 1; 

		char string[128] = {0};
		snprintf(string, sizeof string, "now looking at page %llu of %llu––", 
			jobs[jobpointer].entries[jobs[jobpointer].entrypointer].pagepointer, 
			jobs[jobpointer].entries[jobs[jobpointer].entrypointer].pagecount
		);
		say(string);


	} else if (they_said("where am i")) { 
		if (not jobcount) { say("there are no boxes right now, so your question is not sensical"); return; } 

		char string[128] = {0};
		strlcpy(string, "box pointer is currently set to ", sizeof string);
		strlcat(string, jobs[jobpointer].codename, sizeof string);
		say(string);
	

	} else if (they_said("get the current status")) { 
		if (not jobcount) { say("there are no boxes right now, so there is no status."); return; }

		char string[128] = {0};
		strlcpy(string, "box status is currently set to ", sizeof string);
		strlcat(string, spell_number((nat) jobs[jobpointer].status), sizeof string);
		say(string);


	} else if (they_said("get the current page size")) { 
		if (not jobcount) { say("there are no boxes right now, so there is no status."); return; }

		char string[128] = {0};
		strlcpy(string, "the page size is currently set to ", sizeof string);
		strlcat(string, spell_number(pagesize), sizeof string);
		say(string);


	} else if (they_said("go to next box")) { 
		if (not jobcount) { say("there are no boxes at the moment"); return; } 
		if (jobpointer >= jobcount - 1) { say("already at last box"); return; } 

		jobpointer++; 
		char string[128] = {0};
		strlcpy(string, "now looking at ", sizeof string);
		strlcat(string, jobs[jobpointer].codename, sizeof string);
		say(string);

	} else if (they_said("go to previous box")) { 
		if (not jobcount) { say("there are no boxes at the moment"); return; } 
		if (not jobpointer) { say("already at first box"); return; }

		jobpointer--; 
		char string[128] = {0};
		strlcpy(string, "now looking at ", sizeof string);
		strlcat(string, jobs[jobpointer].codename, sizeof string);
		say(string);

	} else if (they_said("create new box")) { 
		
		jobpointer = jobcount;
		struct job new = {0};
		new.codename = generate_codename(jobpointer);
		new.status = 42;
		
		jobs = realloc(jobs, sizeof(struct job) * (jobcount + 1));
		jobs[jobcount++] = new;
		
		char string[128] = {0};
		strlcpy(string, "creating a box named ", sizeof string);
		strlcat(string, new.codename, sizeof string);
		strlcat(string, " and switching to it", sizeof string);
		say(string); 

	} else if (they_said("list all boxes")) {
		say("the current running boxes are");
		for (nat i = 0; i < jobcount; i++) {
			char string[128] = {0};
			if (jobcount == 1) strlcat(string, "only ", sizeof string);
			else if (i == jobcount - 1) strlcat(string, "and ", sizeof string);
			strlcat(string, "box ", sizeof string);
			strlcat(string, jobs[i].codename, sizeof string);
			if (i < jobcount - 1) strlcat(string, "––", sizeof string);
			say(string);
		}
	}

	for (nat i = 0; i < jobcount; i++) {
		char string[128] = {0};
		strlcpy(string, "switch to ", sizeof string);
		strlcat(string, jobs[i].codename, sizeof string);

		if (they_said(string)) {
			jobpointer = i;
			char response[128] = {0};
			strlcpy(response, "switching to the box named ", sizeof response);
			strlcat(response, jobs[i].codename, sizeof response);
			say(response);
		}
	}
}

static void process_phrase(char* string, nat length) {
	puts(""); string[length] = 0;
	trim_whitespace(&string, &length);
	lowercase_string(string, length);
	nat start = 0, count = 0;
	for (nat index = 0; index < length; index++) {
		if (not isspace(string[index])) {  
			if (not count) start = index; 
			count++; continue; 
		} else if (not count) continue;
		process: process_word(strndup(string + start, count));
		count = 0;
	} if (count) goto process;
}

static void debug_dictionary(void) {
	puts("debugging loaded dictionary...");
	for (nat i = 0; i < dictionary_count / 10; i++) {
		printf("{.word=\"%s\"}    ", dictionary[i]);
		if (i % 8 == 0) puts("");
	}
	puts("[done]");
}

static void load_dictionary(const char* path) {

	printf("info: loading dictionary from \"%s\"...\n", path);
	int file = open(path, O_RDONLY);
	if (file < 0) {
		perror("open");
		puts(path);
		exit(1);
	}
	struct stat s; fstat(file, &s);
	const nat count = (nat) s.st_size;
	char* text = malloc(count);
	read(file, text, count);
	close(file);

	char string[1024] = {0};
	nat length = 0;
	dictionary = NULL;
	dictionary_count = 0;
	nat capacity = 0;
	for (nat i = 0; i < count; i++) {
		if (isspace(text[i])) {
			if (not length) continue;
			char* word = strndup(string, length);
			if (dictionary_count + 1 >= capacity) {
				capacity = 2 * (capacity + 1);
				dictionary = realloc(dictionary, sizeof(char*) * capacity);
			}
			dictionary[dictionary_count++] = word;
			length = 0;
		} else string[length++] = (char) tolower(text[i]);
	}
	free(text);
	printf("[loaded %llu english words]\n", dictionary_count);
}

int main(void) {
	load_dictionary("dictionary_stuff/dictionary.txt");
	// debug_dictionary();

	struct sigaction action2 = {.sa_handler = interrupted}; 
	sigaction(SIGINT, &action2, NULL);
	tcgetattr(0, &terminal);
	struct termios terminal_copy = terminal; 
	terminal_copy.c_cc[VMIN] = 0; 
	terminal_copy.c_cc[VTIME] = phrase_delay_x100ms;  //default: vmin=1,vtime=0
	terminal_copy.c_lflag &= ~((size_t) ICANON);
	tcsetattr(0, TCSAFLUSH, &terminal_copy);
	
	char* phrase = calloc(4096, 1);
	nat length = 0;
	jobpointer = jobcount;
	struct job new = {0};
	new.codename = generate_codename(jobpointer);
	new.status = 12;
	jobs = realloc(jobs, sizeof(struct job) * (jobcount + 1));
	jobs[jobcount++] = new;
	
	puts("voice shell conversation utility written by dwrr on 202403192.210647.");
	input_on();
	say("listening");
	running = true;
	//puts(help_string);
loop:;	char c = 0;
	if (read(0, &c, 1)) { phrase[length++] = c; goto loop; }
	if (length) { process_phrase(phrase, length); length = 0; } 
	if (running) goto loop; 
	tcsetattr(0, TCSAFLUSH, &terminal); 
	input_off();
	exit(0);
}






















/*


//printf("{received (length=%llu): said phrase, \"%s\".\n\n}", length, string);






	say("loaded the following word interpretation");

	char* full_string = NULL;
	nat string_length = 0;
	for (nat i = 0; i < word_count; i++) {
		const nat n = (nat) strlen(words[i]);
		full_string = realloc(full_string, string_length + n + 1);
		memcpy(full_string + string_length, words[i], n);
		string_length += n;
		full_string[string_length++] = ' ';
	}

	full_string = realloc(full_string, string_length + 1);
	full_string[string_length++] = 0;

	puts("<<<");
	puts(full_string);
	puts(">>>");
	say(full_string);










	else if (they_said("go to next output entry")) {
		if (not jobcount) { say("there are no boxes right now"); return; } 
		if (not jobs[jobpointer].entrycount) { say("there are no output entries right now"); return; } 
		if (jobs[jobpointer].entrypointer >= jobs[jobpointer].entrycount - 1) { say("already at the last output entry"); return; }

		jobs[jobpointer].entrypointer++;
		char string[128] = {0};
		strlcpy(string, "now at entry", sizeof string);
		strlcat(string, spell_number(jobs[jobpointer].entrypointer), sizeof string);
		say(string);


	} else if (they_said("go to previous output entry")) {
		if (not jobcount) { say("there are no boxes right now"); return; } 
		if (not jobs[jobpointer].entrycount) { say("there are no output entries right now"); return; } 
		if (not jobs[jobpointer].entrypointer) { say("already at the first output entry"); return; }

		jobs[jobpointer].entrypointer--;
		char string[128] = {0};
		strlcpy(string, "now at entry", sizeof string);
		strlcat(string, spell_number(jobs[jobpointer].entrypointer), sizeof string);
		say(string);













	//	output_information(entry_type_output, 
	//		"static const char* spell_number(nat number) {\n"
	//		"\tchar digits[128] = {0};\n"
	//		"\tconst nat digit_count = (nat) snprintf(digits, sizeof digits, \"%llu\", number);\n"
	//		"\treturn spell_string_number(digits, digit_count);\n"
	//		"}\n");


	//	output_information(entry_type_output,
	//		"\tjobs[jobpointer].entrypointer--;\n"
	//		"\tchar string[128] = {0};\n"
	//		"\tstrlcpy(string, \"now at entry\", sizeof string);\n"
	//		"\tstrlcat(string, spell_number(jobs[jobpointer].entrypointer), sizeof string);\n"
	//		"\tsay(string);\n\n"
	//	);

	//	output_information(entry_type_output, 
	//		"i'm cool lol. 43. 426 . 44.45, this is some 345hello text that i want to be displayed\n"
	//		"it was the output from multiple commands, and is very long indeed.\n"
	//		"i hope you can digest it properly! good luck...\n"
	//		"\tlol.\t\ti'm sure we will get it lol.\n"
	//	);
		
	//	output_information(entry_type_output, 
	//		"\tit was @ i'm : the\t output\nfrom multiple @ commands, and is long.\n"
	//	);

	//	<<<
	//	tab–– it was anthropod–– i'm colon–– the tab–– output newline–– from multiple anthropod–– commands comma–– and is long dot–– newline–– 
	//	>>>









	} else if (they_said("test dictionary")) {
		say("looking up various words in the dictionary");	
		say("bubbles");
		say(is_english_word("bubbles") ? "true" : "false");
		say("daniel");
		say(is_english_word("daniel") ? "true" : "false");
		say("a");
		say(is_english_word("a") ? "true" : "false");
		say("zzz");
		say(is_english_word("zzz") ? "true" : "false");
		say("truth");
		say(is_english_word("truth") ? "true" : "false");
		say("faster");
		say(is_english_word("faster") ? "true" : "false");
		say("english");
		say(is_english_word("english") ? "true" : "false");
		say("dot dot dot");
		say(is_english_word("...") ? "true" : "false");
		say("dot hello");
		say(is_english_word(".hello") ? "true" : "false");
	*/







//struct dictionary_entry {
//	char* word;
//	char* type;
//	char* definition;
//};




//printf("storing word \"%s\"...\n", words[word_count - 1]);
//say(words[word_count - 1]);
//usleep(10000);
// count == 1 ? strdup(spell_character(*word)) : strndup(word, count);


		/*


else if (is_english_word(word + 1)) {
			free(word);
			words = realloc(words, sizeof(char*) * (word_count + 1));
			words[word_count++] = strndup(string + start + 1, count - 1);
			goto next;
		} else {
			const char save = word[count - 1];
			word[count - 1] = 0;
			if (is_english_word(word)) {
				free(word);
				words = realloc(words, sizeof(char*) * (word_count + 1));
				words[word_count++] = strndup(string + start, count - 1);
				goto next;
			}
			word[count - 1] = save;
		}


*/










//char string[128] = {0};
	//strlcpy(string, "creating an initial box named ", sizeof string);
	//strlcat(string, new.codename, sizeof string);
	//strlcat(string, " and switching to it", sizeof string);
	//say(string); 



/*

		//if (text[i] == '\"' and in_quote) in_quote = false;
		//else if (text[i] == '\"' and not in_quote) in_quote = true;	

		//else if (text[i] == ','  and not in_quote) {

		//	if (selector == 0) {
		//	} else if (selector == 1) dictionary[dictionary_count].type = strndup(string, length);
	//		selector++;
			length = 0;
		//} 

		else if (text[i] == '\n' and not in_quote) {

			if (selector == 2) dictionary[dictionary_count].definition = strndup(string, length);
			selector = 0;
			dictionary_count++;
			dictionary = realloc(dictionary, sizeof(struct dictionary_entry) * (dictionary_count + 1));
			length = 0;

		} else {

	*/















	//puts("debugging loaded dictionary...");
	//for (nat i = 0; i < dictionary_count; i++) {
	//	printf("{.word=\"%s\",.type=\"%s\",.def=\"%s\"}\n", dictionary[i].word, dictionary[i].type, dictionary[i].definition);
	//	puts("");
	//}
	//puts("[done]");









/*

else {
		printf("error: unknown word {\"%s\"} ...?\n", word);
		say("unknown word: "); 
		say(word);
	}




static bool stdin_is_empty(void) {
	fd_set f; FD_ZERO(&f); FD_SET(0, &f);
	struct timeval timeout = {0};
	return select(1, &f, 0, 0, &timeout) != 1;
}



static void display(const char** strings) {
	toggle_input();
	for (int i = 0; strings and strings[i]; i++) {
		puts(strings[i]);
		say(strings[i]);
		usleep(10000);
	}
	say("screen end");
	toggle_input();
}








//static char s_file = 0;
//static char s_just = 0;
//static char s_extend = 0;
//static char s_half = 0;





			else if (equals("self", word, wlen))    s_extend = 0; 
			else if (equals("extend", word, wlen))  s_extend = 1; 
			else if (equals("complete", word, wlen))  s_half = 0; 
			else if (equals("split", word, wlen))     s_half = 1; 
			else if (equals("entire", word, wlen)) s_just = 0;
			else if (equals("just", word, wlen))   s_just = 1; 
			else if (equals("file", word, wlen))   s_file = 1;

			else if (equals("reach",    word, wlen)) { if (s_extend) set_anchor(); else clear_anchor();  up(); }
			else if (equals("down",     word, wlen)) { if (s_extend) set_anchor(); else clear_anchor();  down(); }
			
			else if (equals("left",     word, wlen)) { if (s_extend) set_anchor(); else clear_anchor();  left(); }
			else if (equals("right",    word, wlen)) { if (s_extend) set_anchor(); else clear_anchor();  right(); }
			else if (equals("next",     word, wlen)) { if (s_extend) set_anchor(); else clear_anchor();  word_left(); }
			else if (equals("previous", word, wlen)) { if (s_extend) set_anchor(); else clear_anchor();  word_right(); }
			else if (equals("begin",    word, wlen)) { if (s_extend) set_anchor(); else clear_anchor();  up_begin(); }
			else if (equals("end",      word, wlen)) { if (s_extend) set_anchor(); else clear_anchor();  down_end(); }

			else if (equals("page",     word, wlen)) { if (s_extend) set_anchor(); else clear_anchor(); 
								if (s_half) half_page_down(); else page_down(); }
			else if (equals("back",     word, wlen)) { if (s_extend) set_anchor(); else clear_anchor();  
								if (s_half) half_page_up(); else page_up(); }

			else if (equals("line", word, wlen)) { select_current_line(s_just); s_just = 0; }
			else if (equals("word", word, wlen)) { select_current_word(s_just); s_just = 0; }

			else if (equals("stash", word, wlen)) copy(0);
			else if (equals("chuck", word, wlen)) copy(1);
			else if (equals("remove", word, wlen)) cut();
			else if (equals("insert", word, wlen)) paste();

			else if (equals("save", word, wlen) and s_file == 1) { save(); s_file = 0; }










static void select_current_line(char just) {

	nat begin = cursor; 
	while (begin) {
		if (text[begin - 1] == 10) break;
		begin--;
	}
	
	nat end = begin;
	while (end < count) {
		if (text[end] == 10) break;
		end++;
	}

	anchor = begin;
	cursor = end;
	selecting = true;
}

static void select_current_word(char just) {

	nat begin = cursor; 
	while (begin) {
		if (not isalnum(text[begin - 1])) break;
		begin--;
	}
	
	nat end = begin;
	while (end < count) {
		if (not isalnum(text[end])) break;
		end++;
	}

	anchor = begin;
	cursor = end;
	selecting = true;
}






static void execute_from_editor(char* command) {
	const char* string = command;
	const size_t length = strlen(command);
	char** arguments = NULL;
	size_t argument_count = 0;
	size_t start = 0, argument_length = 0;
	for (size_t index = 0; index < length; index++) {
		if (string[index] != 10) {
			if (not argument_length) start = index;
			argument_length++; continue;
		} else if (not argument_length) continue;
	process_word:
		arguments = realloc(arguments, sizeof(char*) * (argument_count + 1));
		arguments[argument_count++] = strndup(string + start, argument_length);
		argument_length = 0;
	}

	if (argument_length) goto process_word;
	arguments = realloc(arguments, sizeof(char*) * (argument_count + 1));
	arguments[argument_count] = NULL;
	write(1, "\033[?25h\033[?1049l", 14);
	tcsetattr(0, TCSAFLUSH, &terminal);
	create_process(arguments);
	struct termios terminal_copy = terminal; 
	terminal_copy.c_iflag &= ~((size_t) IXON);
	terminal_copy.c_lflag &= ~((size_t) ECHO | ICANON);
	tcsetattr(0, TCSAFLUSH, &terminal_copy);
	write(1, "\033[?1049h\033[?25l", 8);
	free(arguments);
}















static void left(void) { 
	if (cursor) cursor--; 
	if (cursor < origin) {
		if (origin) origin--;
		if (origin) origin--;
		while (origin and text[origin] != 10) origin--;
		if (origin and origin < count) origin++;
		display(0);
	} moved = 1;
}

static void right(void) { 
	if (cursor < count) cursor++;
	if (cursor >= finish) {
		while (origin < count and text[origin] != 10) origin++;
		if (origin < count) origin++;
		display(0);
	} moved = 1;
}

static nat compute_current_visual_cursor_column(void) {
	nat i = cursor, column = 0;
	while (i and text[i - 1] != 10) i--;
	while (i < cursor and text[i] != 10) {
		if (text[i] == 9) { nat amount = 8 - column % 8; column += amount; }
		else if (column >= window.ws_col - 2 - 1) column = 0;
		else if ((unsigned char) text[i] >> 6 != 2 and text[i] >= 32) column++;
		i++;
	}
	return column;
}

static void move_cursor_to_visual_position(nat target) {
	nat column = 0;
	while (cursor < count and text[cursor] != 10) {
		if (column >= target) return;
		if (text[cursor] == 9) { nat amount = 8 - column % 8; column += amount; }
		else if (column >= window.ws_col - 2 - 1) column = 0;
		else if ((unsigned char) text[cursor] >> 6 != 2 and text[cursor] >= 32) column++;
		right();
	}
}

static void up(void) {
	const bool m = moved; 
	const nat column = compute_current_visual_cursor_column();
	while (cursor and text[cursor - 1] != 10) left(); left();
	while (cursor and text[cursor - 1] != 10) left();
	move_cursor_to_visual_position(not m ? desired : column);
	if (m) desired = column;
	moved = 0;
}

static void down(void) {
	const bool m = moved; 
	const nat column = compute_current_visual_cursor_column();
	while (cursor < count and text[cursor] != 10) right(); right();
	move_cursor_to_visual_position(not m ? desired : column);
	if (m) desired = column;
	moved = 0;
}

static void up_begin(void) {
	while (cursor) {
		left();
		if (not cursor or text[cursor - 1] == 10) break;
	}
}

static void down_end(void) {
	while (cursor < count) {
		right();
		if (cursor >= count or text[cursor] == 10) break;
	}
}

static void word_left(void) {
	left();
	while (cursor) {
		if (not (not isalnum(text[cursor]) or isalnum(text[cursor - 1]))) break;
		if (text[cursor - 1] == 10) break;
		left();
	}
}

static void word_right(void) {
	right();
	while (cursor < count) {
		if (not (isalnum(text[cursor]) or not isalnum(text[cursor - 1]))) break;
		if (text[cursor] == 10) break;
		right();
	}
}

static void searchf(void) {
	nat t = 0;
	loop: if (t == cliplength or cursor >= count) return;
	if (text[cursor] != clipboard[t]) t = 0; else t++; 
	right(); goto loop;
}

static void searchb(void) {
	nat t = cliplength;
	loop: if (not t or not cursor) return;
	left(); t--; 
	if (text[cursor] != clipboard[t]) t = cliplength;
	goto loop;
}

static void write_file(const char* directory, char* name, size_t maxsize) {
	int flags = O_WRONLY | O_TRUNC;
	mode_t permission = 0;
	if (not *name) {
		srand((unsigned)time(0)); rand();
		char datetime[32] = {0};
		struct timeval t = {0};
		gettimeofday(&t, NULL);
		struct tm* tm = localtime(&t.tv_sec);
		strftime(datetime, 32, "1%Y%m%d%u.%H%M%S", tm);
		snprintf(name, maxsize, "%s%s_%08x%08x.txt", directory, datetime, rand(), rand());
		flags |= O_CREAT | O_EXCL;
		permission = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
	}
	int file = open(name, flags, permission);
	if (file < 0) { perror("save: open file"); puts(name); getchar(); }
	write(file, text, count);
	close(file);
}

static void save(void) {    
	write_file("./", filename, sizeof filename); 
	if (autosave_counter < autosave_frequency) return;
	write_file(autosave_directory, autosavename, sizeof autosavename); 
	autosave_counter = 0;
}

static void finish_action(struct action node, char c) {
	node.post = cursor; node.c = c;
	head = action_count;
	actions = realloc(actions, sizeof(struct action) * (action_count + 1));
	actions[action_count++] = node;
}

static void insert(char c, bool should_record) {
	if (should_record) autosave_counter++;
	if (autosave_counter >= autosave_frequency and should_record) save();
	struct action node = { .parent = head, .pre = cursor, .inserting = 1, .choice = 0 };
	text = realloc(text, count + 1);
	memmove(text + cursor + 1, text + cursor, count - cursor);
	text[cursor] = c; count++; right();
	if (should_record) finish_action(node, c);
}

static char delete(bool should_record) {
	struct action node = { .parent = head, .pre = cursor, .inserting = 0, .choice = 0 };
	left(); count--; char c = text[cursor];
	memmove(text + cursor, text + cursor + 1, count - cursor);
	text = realloc(text, count);
	if (should_record) finish_action(node, c);
	return c;
}

static void cut(void) {
	if (not selecting or anchor == (nat) ~0) return;
	if (anchor > count) anchor = count;
	if (anchor > cursor) { nat t = anchor; anchor = cursor; cursor = t; }
	free(clipboard);
	clipboard = strndup(text + anchor, cursor - anchor);
	cliplength = cursor - anchor;
	for (nat i = 0; i < cliplength and cursor; i++) delete(1);
	anchor = (nat) ~0;
	selecting = 0;
}

static void redo(void) {
	nat chosen_child = 0, child_count = 0; 
	for (nat i = 0; i < action_count; i++) {
		if (actions[i].parent != head) continue;
		if (child_count == actions[head].choice) chosen_child = i;
		child_count++;
	}
	if (not child_count) return;
	if (child_count >= 2) {
		printf("\n\033[7m[      %u  :  %llu      ]\033[0m\n", 
			actions[head].choice, child_count
		); getchar(); 
		actions[head].choice = (actions[head].choice + 1) % child_count;
	}
	head = chosen_child;
	const struct action node = actions[head];
	cursor = node.pre; 
	if (node.inserting) insert(node.c, 0); else delete(0);
	cursor = node.post;
}

static void undo(void) {
	if (not head) return;
	struct action node = actions[head];
	cursor = node.post;
	if (node.inserting) delete(0); else insert(node.c, 0); 
	cursor = node.pre;
	head = node.parent;
}

static inline void copy(bool should_delete) {
	if (not selecting or anchor == (nat) ~0) return;
	if (anchor > count) anchor = count;
	cliplength = anchor < cursor ? cursor - anchor : anchor - cursor;
	clipboard = strndup(text + (anchor < cursor ? anchor : cursor), cliplength);
	FILE* globalclip = popen("pbcopy", "w");
	if (not globalclip) {
		perror("copy popen pbcopy");
		getchar(); return;
	}	
	fwrite(clipboard, 1, cliplength, globalclip);
	pclose(globalclip);
	if (should_delete) cut();
}

static void insert_output(const char* input_command) {
	save();
	char command[4096] = {0};
	strlcpy(command, input_command, sizeof command);
	strlcat(command, " 2>&1", sizeof command);

	FILE* f = popen(command, "r");
	if (not f) {
		printf("error: could not execute \"%s\"\n", command);
		perror("insert_output popen");
		getchar(); return;
	}
	char* string = NULL;
	size_t length = 0;
	char line[2048] = {0};
	while (fgets(line, sizeof line, f)) {
		size_t l = strlen(line);
		string = realloc(string, length + l);
		memcpy(string + length, line, l);
		length += l;
	}
	pclose(f);
	for (nat i = 0; i < length; i++) insert(string[i], 1);
	free(string);
}

static void window_resized(int _) {if(_){} ioctl(0, TIOCGWINSZ, &window); }
static noreturn void interrupted(int _) {if(_){} 
	write(1, "\033[?25h\033[?1049l", 14);
	tcsetattr(0, TCSAFLUSH, &terminal);
	save(); exit(0); 
}

static void change_directory(const char* d) {
	if (chdir(d) < 0) {
		perror("change directory chdir");
		printf("directory=%s\n", d);
		getchar(); return;
	}
	printf("changed to %s\n", d);
	getchar();
}

static void create_process(char** args) {
	pid_t pid = fork();
	if (pid < 0) { perror("fork"); getchar(); return; }
	if (not pid) {
		if (execve(args[0], args, environ) < 0) { perror("execve"); exit(1); }
	} 
	int status = 0;
	if ((pid = wait(&status)) == -1) { perror("wait"); getchar(); return; }
	char dt[32] = {0};
	struct timeval t = {0};
	gettimeofday(&t, NULL);
	struct tm* tm = localtime(&t.tv_sec);
	strftime(dt, 32, "1%Y%m%d%u.%H%M%S", tm);
	if (WIFEXITED(status)) 		printf("[%s:(%d) exited with code %d]\n", dt, pid, WEXITSTATUS(status));
	else if (WIFSIGNALED(status)) 	printf("[%s:(%d) was terminated by signal %s]\n", dt, pid, strsignal(WTERMSIG(status)));
	else if (WIFSTOPPED(status)) 	printf("[%s:(%d) was stopped by signal %s]\n", 	dt, pid, strsignal(WSTOPSIG(status)));
	else 				printf("[%s:(%d) terminated for an unknown reason]\n", dt, pid);
	fflush(stdout);
	getchar();
}

static void execute(char* command) {
	save();
	const char* string = command;
	const size_t length = strlen(command);
	char** arguments = NULL;
	size_t argument_count = 0;
	size_t start = 0, argument_length = 0;
	for (size_t index = 0; index < length; index++) {
		if (string[index] != 10) {
			if (not argument_length) start = index;
			argument_length++; continue;
		} else if (not argument_length) continue;
	process_word:
		arguments = realloc(arguments, sizeof(char*) * (argument_count + 1));
		arguments[argument_count++] = strndup(string + start, argument_length);
		argument_length = 0;
	}

	if (argument_length) goto process_word;
	arguments = realloc(arguments, sizeof(char*) * (argument_count + 1));
	arguments[argument_count] = NULL;
	write(1, "\033[?25h\033[?1049l", 14);
	tcsetattr(0, TCSAFLUSH, &terminal);
	create_process(arguments);
	struct termios terminal_copy = terminal; 
	terminal_copy.c_iflag &= ~((size_t) IXON);
	terminal_copy.c_lflag &= ~((size_t) ECHO | ICANON);
	tcsetattr(0, TCSAFLUSH, &terminal_copy);
	write(1, "\033[?1049h\033[?25l", 8);
	free(arguments);
}

static void jump_index(char* string) {
	const size_t n = (size_t) atoi(string);
	for (size_t i = 0; i < n; i++) right();
}

static void jump_line(char* string) {
	const size_t n = (size_t) atoi(string);
	cursor = 0; origin = 0;
	for (size_t i = 0; i < n; i++) down_end();
	up_begin(); 
}

static void set_anchor(void) { if (selecting) return; anchor = cursor;  selecting = 1; }
static void clear_anchor(void) { if (not selecting) return; anchor = (nat) ~0; selecting = 0; }
static void paste(void) { if (selecting) cut(); insert_output("pbpaste"); }
static void local_paste(void) { for (nat i = 0; i < cliplength; i++) insert(clipboard[i], 1); }
static void insert_string(const char* string) { for (nat i = 0; i < strlen(string); i++) insert(string[i], 1); }

static void page_up(void)   { for (int i = 0; i < window.ws_row - 3; i++) up(); } 
static void page_down(void) { for (int i = 0; i < window.ws_row - 3; i++) down(); } 
static void half_page_up(void)   { for (int i = 0; i < (window.ws_row - 3) / 2; i++) up(); } 
static void half_page_down(void) { for (int i = 0; i < (window.ws_row - 3) / 2; i++) down(); } 

static void interpret_arrow_key(void) {
	char c = 0; 
	read(0, &c, 1);
	     if (c == 'u') { clear_anchor(); up_begin(); }
	else if (c == 'd') { clear_anchor(); down_end(); }
	else if (c == 'l') { clear_anchor(); word_left(); }
	else if (c == 'r') { clear_anchor(); word_right(); }
	else if (c == 'f') { clear_anchor(); searchf(); }
	else if (c == 'b') { clear_anchor(); searchb(); }
	else if (c == 't') { clear_anchor(); page_up(); }
	else if (c == 'e') { clear_anchor(); page_down(); }
	else if (c == 's') {
		read(0, &c, 1); 
		     if (c == 'u') { set_anchor(); up(); }
		else if (c == 'd') { set_anchor(); down(); }
		else if (c == 'r') { set_anchor(); right(); }
		else if (c == 'l') { set_anchor(); left(); }
		else if (c == 'b') { set_anchor(); up_begin(); }
		else if (c == 'e') { set_anchor(); down_end(); }
		else if (c == 'w') { set_anchor(); word_right(); }
		else if (c == 'm') { set_anchor(); word_left(); }
	} else if (c == '[') {
		read(0, &c, 1); 
		if (c == 'A') { clear_anchor(); up(); }
		else if (c == 'B') { clear_anchor(); down(); }
		else if (c == 'C') { clear_anchor(); right(); }
		else if (c == 'D') { clear_anchor(); left(); }
		else { printf("error: found escape seq: ESC [ #%d\n", c); getchar(); }
	} else { printf("error found escape seq: ESC #%d\n", c); getchar(); }
}

int main(int argc, const char** argv) {
	struct sigaction action = {.sa_handler = window_resized}; 
	sigaction(SIGWINCH, &action, NULL);
	struct sigaction action2 = {.sa_handler = interrupted}; 
	sigaction(SIGINT, &action2, NULL);
	if (argc < 2) goto new;
	strlcpy(filename, argv[1], sizeof filename);
	int df = open(filename, O_RDONLY | O_DIRECTORY);
	if (df >= 0) { close(df); errno = EISDIR; goto read_error; }
	int file = open(filename, O_RDONLY);
	if (file < 0) { read_error: perror("load: read open file"); exit(1); }
	struct stat s; fstat(file, &s);
	count = (nat) s.st_size;
	text = malloc(count);
	read(file, text, count);
	close(file);
new: 	origin = 0; cursor = 0; anchor = (nat) ~0;
	finish_action((struct action){.parent = (nat) ~0}, 0);
	tcgetattr(0, &terminal);
	struct termios terminal_copy = terminal; 
	terminal_copy.c_cc[VMIN] = 1; 
	terminal_copy.c_cc[VTIME] = 0;  //vmin=1,vtime=0   
	terminal_copy.c_iflag &= ~((size_t) IXON);
	terminal_copy.c_lflag &= ~((size_t) ECHO | ICANON);
	tcsetattr(0, TCSAFLUSH, &terminal_copy);
	write(1, "\033[?1049h\033[?25l", 14);
loop:	display(1);
	char c = 0;
	read(0, &c, 1);
	     if (c == 17) goto do_c;	// Q
	else if (c == 19) save();	// S
	else if (c == 18) redo(); 	// R
	else if (c == 4)  undo(); 	// D
	else if (c == 8)  copy(0); 	// H
	else if (c == 24) copy(1); 	// X
	else if (c == 1)  paste();	// A
	else if (c == 20) local_paste();// T
	else if (c == 27) interpret_arrow_key();
	else if (c == 127) { if (selecting) cut(); else if (cursor) delete(1); }
	if ((unsigned char) c >= 32 or c == 10 or c == 9) { if (selecting) cut(); insert(c, 1); }
	else { printf("error: ignoring input byte '%d'", c); fflush(stdout); getchar(); } 
	goto loop;
do_c:	if (not cliplength) goto loop;
	else if (not strcmp(clipboard, "exit")) goto done;
	else if (not strncmp(clipboard, "insert ", 7)) insert_output(clipboard + 7);
	else if (not strncmp(clipboard, "change ", 7)) change_directory(clipboard + 7);
	else if (not strncmp(clipboard, "do ", 3)) execute(clipboard + 3);
	else if (not strncmp(clipboard, "index ", 6)) jump_index(clipboard + 6);
	else if (not strncmp(clipboard, "line ", 5)) jump_line(clipboard + 5);	
	else { printf("unknown command: %s\n", clipboard); getchar(); }
	goto loop;
done:	write(1, "\033[?25h\033[?1049l", 14);
	tcsetattr(0, TCSAFLUSH, &terminal);
	save(); exit(0);
}












































static void change_directory(const char* d) {
        if (chdir(d) < 0) {
                perror("change directory chdir");
                printf("directory=%s\n", d);
                getchar(); return;
        }
	display((const char*[]) {"changed to", d, 0});
}

static void print_strings(const char** arguments, size_t argument_count) {
	printf("argv(argc=%lu):\n", argument_count);
        for (size_t i = 0; i < argument_count; i++) {
                printf("\targument#%lu: \"%s\"\n", i, arguments[i]);
        }
        printf("[end of args]\n");
}

static void print_strings_mutable(char** arguments, size_t argument_count) {
	printf("argv(argc=%lu):\n", argument_count);
        for (size_t i = 0; i < argument_count; i++) {
                printf("\targument#%lu: \"%s\"\n", i, arguments[i]);
        }
        printf("[end of args]\n");
}

static void run_command(const char* input_command) {

        char command[4096] = {0};
        strlcpy(command, input_command, sizeof command);
        strlcat(command, " 2>&1", sizeof command);

	display((const char*[]) {"executing", command, 0});

        FILE* f = popen(command, "r");
        if (not f) {
                printf("error: could not execute \"%s\"\n", command);
                perror("insert_output popen");
		screen = (const char*[]) {"conversation error, could not execute command using popen", 0};
		display(screen);
        }
        char* string = NULL;
        size_t length = 0;
        char line[2048] = {0};
        while (fgets(line, sizeof line, f)) {
                size_t l = strlen(line);
                string = realloc(string, length + l);
                memcpy(string + length, line, l);
                length += l;
        }
        pclose(f);


  	printf("splitting \"%s\"...\n", strndup(string, length));

        const char** arguments = NULL;
        size_t argument_count = 0;

        size_t start = 0, argument_length = 0;

        for (size_t index = 0; index < length; index++) {

                if (string[index] != 10) {
                        if (not argument_length) start = index;
                        argument_length++; continue;
                } else if (not argument_length) continue;

        process_word:
                arguments = realloc(arguments, sizeof(const char*) * (argument_count + 1));
                arguments[argument_count++] = strndup(string + start, argument_length);
                argument_length = 0;
        }

        if (argument_length) goto process_word;

        arguments = realloc(arguments, sizeof(const char*) * (argument_count + 1));
        arguments[argument_count] = NULL;

	print_strings(arguments, argument_count);

        screen = arguments;

	display(screen);
}


static void execute(char* command) {

        display((const char*[]) {"executing", command, 0});

        const char* string = command;
        const size_t length = strlen(command);
        char** arguments = NULL;
        size_t argument_count = 0;
        size_t start = 0, argument_length = 0;
        for (size_t index = 0; index < length; index++) {
                if (string[index] != 1) {
                        if (not argument_length) start = index;
                        argument_length++; continue;
                } else if (not argument_length) continue;
        process_word:
                arguments = realloc(arguments, sizeof(char*) * (argument_count + 1));
                arguments[argument_count++] = strndup(string + start, argument_length);
                argument_length = 0;
        }
        if (argument_length) goto process_word;

        arguments = realloc(arguments, sizeof(char*) * (argument_count + 1));
        arguments[argument_count] = NULL;

        print_strings_mutable(arguments, argument_count);

	create_process(arguments);
        free(arguments);
}







//    else insert_string("\n error: unknown command\n");
//printf("FILE-SAVE{\"%.*s\"}, ", (int) wlen, word);
//printf("ERROR:state_file[%.*s], ", (int) wlen, word);
//printf("HELLO{\"%.*s\"}, ", (int) wlen, word);
//printf("CUT{\"%.*s\"}, ", (int) wlen, word);
//printf("FILE{\"%.*s\"}, ", (int) wlen, word);
//printf("COPY{\"%.*s\"}, ", (int) wlen, word);
//printf("PASTE{\"%.*s\"}, ", (int) wlen, word);
//printf("CUT{\"%.*s\"}, ", (int) wlen, word);
//printf("[%.*s], ", (int) wlen, word);
//printf("\033[K\n[DONE PROCESSING!]\033[K\n");

//fflush(stdout);



	//char input[1024] = {0};
	//fgets(input, sizeof input, stdin);
	//input[strlen(input) - 1] = 0;

	// mode = 0;



	//else if (not strcmp(clipboard, "voice")) mode = voice_mode;
	//else if (not strcmp(clipboard, "keyboard")) mode = keyboard_mode;



//	mode = voice_mode;
	




	//} else {
			
	//	goto loop;
	//}




	







	if (not strcmp(input, "exit")) exit(0);
	else if (not strcmp(input, "say hello")) { display(screen = (const char*[]) {"hello there from space", "this is a test.", 0}); }
	else if (not strcmp(input, "again")) display(screen);
	else if (not strcmp(input, "help")) {
		display(screen = (const char*[]) {
			"commands include the following",
			"exit",
			"say hello",
			"again",
			"help",
			"run",
			"change",
			"do",
		0});
	}
        else if (not strncmp(input, "run ", 4)) run_command(input + 4); 
        else if (not strncmp(input, "change ", 7)) change_directory(input + 7);
        else if (not strncmp(input, "execute ", 8)) execute(input + 8);
        else display((const char*[]) {"unknown command", input, 0});










        //const size_t length = strlen(text);
        //char* command = calloc(length + 10, 1);
        //snprintf(command, sizeof command, "/usr/bin/say\01%s\01", text);
        //free(command);






*/

































