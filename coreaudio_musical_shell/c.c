// musical shell/editor
// written on 1202412172.003209
// by dwrr 
// made to use coreaudio on 1202412172.013059
// old: 
//  example sine wave gneration in 
//  sdl2 in c.
//  dwrr
//  written on 1202407276.155019
//  202407276.212254: a music sine synthesizer

#include <AudioUnit/AudioUnit.h>

#include <iso646.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>

enum note_state { sustained, rising, falling };

static const int sample_rate = 44100;
static const float tau = (float) (2.0 * M_PI);

static const float start_vol = 0.00005f;
static const float cut_off_vol = 0.00001f;
static const float max_vol = 0.05f;

static const float start_speed = 0.0032f;
static const float decay_speed = 0.00003f;

enum note_names { 
	C4, Db4, D4, Eb4, E4, F4, Gb4, G4, Ab4, A4, Bb4, B4,
	C5, Db5, D5, Eb5, E5, F5, Gb5, G5, Ab5, A5, Bb5, B5,
	C6, Db6, D6, Eb6, E6, F6, Gb6, G6, Ab6, A6, Bb6, B6,
	C7, Db7, D7, Eb7, E7, F7, Gb7, G7, Ab7, A7, Bb7, B7,
note_count

};



static const float freq[note_count] = {
	261.626f, 277.183f, 293.665f, 311.184f, 329.628f, 349.228f, 369.994f, 391.995f, 415.305f, 440.000f, 466.164f, 493.883f,
	2 * 261.626f, 2 * 277.183f, 2 * 293.665f, 2 * 311.184f, 2 * 329.628f, 2 * 349.228f, 2 * 369.994f, 2 * 391.995f, 2 * 415.305f, 2 * 440.000f, 2 * 466.164f, 2 * 493.883f,

	4 * 261.626f, 4 * 277.183f, 4 * 293.665f, 4 * 311.184f, 4 * 329.628f, 4 * 349.228f, 4 * 369.994f, 4 * 391.995f, 4 * 415.305f, 4 * 440.000f, 4 * 466.164f, 4 * 493.883f,

	8 * 261.626f, 8 * 277.183f, 8 * 293.665f, 8 * 311.184f, 8 * 329.628f, 8 * 349.228f, 8 * 369.994f, 8 * 391.995f, 8 * 415.305f, 8 * 440.000f, 8 * 466.164f, 8 * 493.883f,
};

struct note {
	float step;
	float volume;
};

static struct note notes[note_count] = {0};
static uint8_t state[note_count] = {0};
static struct termios terminal = {0};

static OSStatus audio_callback(void* inRefCon, 
	AudioUnitRenderActionFlags* ioActionFlags,
	const AudioTimeStamp *inTimeStamp,
	UInt32 inBusNumber,
	UInt32 inNumberFrames,
	AudioBufferList *ioData
) {
	SInt16* left = (SInt16*) ioData->mBuffers[0].mData;
	for (UInt32 frame = 0; frame < inNumberFrames; frame++) {

		float output = 0.0;
		for (int n = 0; n < note_count; n++) {

			output += sinf(notes[n].step) * notes[n].volume;
			notes[n].step = fmodf(notes[n].step + (tau * freq[n] / sample_rate), tau);

			if (state[n] == falling) {

				if (notes[n].volume > cut_off_vol) 
					notes[n].volume *= (1 - decay_speed);
				else { 
					state[n] = sustained; 
					notes[n].volume = 0.0f; 
				}

			} else if (state[n] == rising) {

				if (notes[n].volume < max_vol) { 
					if (notes[n].volume < start_vol) 
						notes[n].volume += start_vol; 
					notes[n].volume *= (1 + start_speed); 
				} else { 
					state[n] = sustained; 
					notes[n].volume = max_vol; 
				}
			}
		}
		left[frame] = (SInt16)(output * 32767.0f);
	}
	memcpy(ioData->mBuffers[1].mData, left, ioData->mBuffers[1].mDataByteSize);
	return noErr;
}

static int translate(char c) {
	if (c == 0) return 1;
	if (c == 1) return -1;
	if (c == 2) return 2;
	if (c == 3) return -2;
	if (c == 4) return 3;
	if (c == 5) return -3;
	if (c == 6) return 4;
	if (c == 7) return -4;
	if (c == 8) return 5;
	if (c == 9) return -5;
	if (c == 10) return 6;
	if (c == 11) return -6;
	if (c == 12) return 7;
	if (c == 13) return -7;
	if (c == 14) return 8;
	if (c == 15) return -8;

	return 0;
}

static char hex(const char c) {
	if (c >= '0' and c <= '9') return c - '0';
	else return 10 + c - 'A';
}

static void display(const char* input) {

	const int length = (int) strlen(input);

	int end = 0; 
	for (int i = 0; i < length; i++) {

		const char c = input[i];
		const int interval = translate(hex(c));
		int start = 12;
		if (start == end) start++;
		end = start + interval;		

		state[start] = rising; 
		usleep(200000); 
		state[start] = falling;
		state[end] = rising; 
		usleep(500000); 
		state[end] = falling; 
		usleep(100000); 
	}
}

/*


[timeindex :] note [. length]          
[timeindex :] note [. length]          
[timeindex :] note [. length]          



this would be a major scale, played evenly. 
	i think note "3" is D natural, so yeah. 

0: 3 . 1
5
7
8
10
12
14
15


note:
	timestep and length is in multiples of set minimum!

	default length is the same as the previous length, 
	default timestep is the next one. (incremented)

*/




static void play_piece(const char* string, size_t length) {

	size_t line_start = 0, line_length = 0;
	size_t current_length = 1;
	size_t current_timestep = 0;
	size_t* piece = NULL;
	size_t piece_length = 0;
	size_t tempo = 500000;

	for (size_t i = 0; i < length; i++) {
		const char c = string[i];
		if (c == 10) {
			//line_length++;

			printf("looking at line from %lu, count %lu\n", line_start, line_length);
			printf(" --> (%lu)<<<", line_length);
			for (size_t e = 0; e < line_length; e++) {
				printf("%c", string[e + line_start]);
			}
			puts(">>>");


			if (line_length == 0) goto line_done;

			const char* line = string + line_start;


			if (line[0] == 'T' and line[1] == ' ') {
				char number[16] = {0};
				size_t number_length = 0;

				puts("FOUND TEMPO MARKING!!!");

				for (size_t start = 0; start < line_length; start++) {
					if (isdigit(line[start])) number[number_length++] = line[start];
				}
				number[number_length] = 0;
				tempo = (size_t) atoi(number);				
				
				goto line_done;
			}

			size_t this_timestep = current_timestep + current_length;
			size_t this_note = (size_t) -1;
			size_t this_length = current_length;

			char contains_length = 0;
			char contains_timestep = 0;

			for (size_t e = 0; e < line_length; e++) {
				if (line[e] == '.') contains_length = 1;
				if (line[e] == ':') contains_timestep = 1;
			}

			char number[16] = {0};
			size_t number_length = 0;
			size_t start = 0;
			if (contains_timestep) {				
				for (; start < line_length; start++) {
					if (line[start] == ':') break;
					if (isdigit(line[start])) number[number_length++] = line[start];
				}
				number[number_length] = 0;
				this_timestep = (size_t) atoi(number);				
			}
			
			number_length = 0;
			for (; start < line_length; start++) {
				if (line[start] == '.') break;
				if (isdigit(line[start])) number[number_length++] = line[start];
			}
			number[number_length] = 0;
			if (number_length) this_note = (size_t) atoi(number);

			if (contains_length) {
				number_length = 0;
				for (; start < line_length; start++) {
					if (isdigit(line[start])) number[number_length++] = line[start];
				}
				number[number_length] = 0;
				this_length = (size_t) atoi(number);				
			}

			if (this_note == (size_t) -1) { 
				printf("ignoring line...\n"); 
				goto line_done; 
			}
			
			current_timestep = this_timestep;
			current_length = this_length;

			printf("parsed this note: { .timestep = %lu, .note = %lu, .length = %lu }\n", 
				this_timestep, this_note, this_length
			);

			if (this_note and this_note - 1 >= note_count) { puts("invalid note name/index"); abort(); }

			piece = realloc(piece, sizeof(size_t) * 3 * (piece_length + 1));	
			piece[3 * piece_length + 0] = this_timestep;
			piece[3 * piece_length + 1] = this_note;
			piece[3 * piece_length + 2] = this_length;
			piece_length++;
		
		line_done:
			line_start = i + 1;
			line_length = 0;
		} else {
			line_length++;
		}
	}

	puts("playing notes..\n");
	size_t note_usages[256] = {0};

	for (size_t t = 0; true; t++) {

		//printf("timestep %lu:\n", t);

		for (size_t i = 0; i < piece_length; i++) {
			const size_t this_timestep = piece[3 * i + 0];
			const size_t this_note = piece[3 * i + 1];
			const size_t this_length = piece[3 * i + 2];

			if (this_timestep == t) {
				//puts("note start");
				if (not note_usages[this_note]) {
					//puts("note rising!");
					if (this_note) state[this_note - 1] = rising;
				}
				note_usages[this_note]++;
			}

			if (this_timestep + this_length == t) {
				//puts("note end");
				if (note_usages[this_note] == 1) {
					//puts("note falling!");
					if (this_note) state[this_note - 1] = falling;
				}
				if (note_usages[this_note]) note_usages[this_note]--; else { puts("note invalid usage"); abort(); }
			}
		}

		usleep(tempo);

		for (int i = 0; i < 256; i++) {
			if (note_usages[i]) { 
				//puts("found nonzero note usage"); 
				goto next_ts; 
			}
		}
		//puts("note usages were all zero! breaking out...");
		if (t >= piece[3 * (piece_length - 1)]) break;
	next_ts:;
	}

	exit(0);
}
















/*



		const int interval = translate(hex(c));
		int start = 12;
		if (start == end) start++;
		end = start + interval;		

		state[start] = rising; 
		usleep(200000); 
		state[start] = falling;
		state[end] = rising; 
		usleep(500000); 
		state[end] = falling; 
		usleep(100000); 


*/

int main(int argc, const char** argv) {

	bool should_play_piece = 0;
	size_t text_length = 0;
	char* text = NULL;

	if (argc == 1) goto skip_file;
	int file = open(argv[1], O_RDONLY);
	if (file < 0) { perror("open"); exit(1); }
	text_length = (size_t) lseek(file, 0, SEEK_END);
	lseek(file, 0, SEEK_SET);
	text = malloc(text_length);	
	read(file, text, text_length);
	close(file);
	printf("<<<%.*s>>>\n", (int) text_length, text);
	should_play_piece = true;
skip_file:;
	tcgetattr(0, &terminal);
	struct termios terminal_copy = terminal; 
	terminal_copy.c_cc[VMIN] = 1;
	terminal_copy.c_cc[VTIME] = 0;
	terminal_copy.c_lflag &= ~((size_t) (ICANON | ECHO));
	tcsetattr(0, TCSAFLUSH, &terminal_copy);

	AudioComponentDescription description = {
		.componentType = kAudioUnitType_Output,
		.componentSubType = kAudioUnitSubType_DefaultOutput,
		.componentManufacturer = kAudioUnitManufacturer_Apple,
	};

	AudioComponent output = AudioComponentFindNext(NULL, &description);
	if (!output) { printf("Can't find default output\n"); abort(); } 

	AudioUnit unit;
	OSStatus err = AudioComponentInstanceNew(output, &unit);
	if (err) { printf("error: could not create audio unit: %d\n", err); abort(); }

	AURenderCallbackStruct input = { .inputProc = audio_callback };
	err = AudioUnitSetProperty(
		unit, 
		kAudioUnitProperty_SetRenderCallback, 
		kAudioUnitScope_Input, 
		0, &input, sizeof input
	);
	if (err) { printf("error: could not set callback: %d\n", err); abort(); } 

	AudioStreamBasicDescription stream = {
		.mFormatID = kAudioFormatLinearPCM,
		.mFormatFlags = 0
		| kAudioFormatFlagIsSignedInteger
		| kAudioFormatFlagIsPacked
		| kAudioFormatFlagIsNonInterleaved,
		.mSampleRate = 48000,
		.mBitsPerChannel = 16,
		.mChannelsPerFrame = 2,
		.mFramesPerPacket = 1,
		.mBytesPerFrame = 2,
		.mBytesPerPacket = 2,
	};

	err = AudioUnitSetProperty(unit, kAudioUnitProperty_StreamFormat,
            kAudioUnitScope_Input, 0, &stream, sizeof stream);
	if (err) printf("Error setting stream format: %d\n", err);

	err = AudioUnitInitialize(unit);
	if (err) printf("Error initializing unit: %d\n", err);

	err = AudioOutputUnitStart(unit);
	if (err) printf("Error starting unit: %d\n", err);



	if (should_play_piece) {
		play_piece(text, text_length);
		exit(0);
	}

	while (1) {
		char c = 0;
		read(0, &c, 1);
		if (c == 'q') break;
/*
		if (c == 'a') { state[0] = rising; } 
		if (c == 'd') { state[0] = falling; } 

		if (c == 'r') { state[4] = rising; }  
		if (c == 't') { state[4] = falling; } 

		if (c == 'i') { state[7] = rising; } 
		if (c == 'p') { state[7] = falling; } 

		if (c == 'u') { state[11] = rising; } 
		if (c == 'n') { state[11] = falling; } 


		if (c == 'm') { state[14] = rising; } 
		if (c == 'c') { state[14] = falling; } 

		if (c == 'l') { state[18] = rising; }  
		if (c == 'k') { state[18] = falling; } 

		if (c == 'g') { state[21] = rising; } 
		if (c == 'y') { state[21] = falling; } 

		if (c == 'b') { state[25] = rising; } 
		if (c == 'j') { state[25] = falling; } 

*/

		if (c == 'a') display("012");

		if (c == 'i') display("528");

		if (c == 'n') display("F3819");

		if (c == ' ') { state[0] = falling; } 
	}

	AudioOutputUnitStop(unit);
	AudioUnitUninitialize(unit);
	AudioComponentInstanceDispose(unit);
	tcsetattr(0, TCSANOW, &terminal);
}







//	float* output = (float*) stream;
//	for (int o = 0; o < sample_count * 2; o++) {




/*



		if (c == 'd') { state[0] = falling; } 

		if (c == 'r') { state[4] = rising; }  
		if (c == 't') { state[4] = falling; } 

		if (c == 'i') { state[7] = rising; } 
		if (c == 'p') { state[7] = falling; } 

		if (c == 'u') { state[11] = rising; } 
		if (c == 'n') { state[11] = falling; } 


		if (c == 'm') { state[14] = rising; } 
		if (c == 'c') { state[14] = falling; } 

		if (c == 'l') { state[18] = rising; }  
		if (c == 'k') { state[18] = falling; } 

		if (c == 'g') { state[21] = rising; } 
		if (c == 'y') { state[21] = falling; } 

		if (c == 'b') { state[25] = rising; } 
		if (c == 'j') { state[25] = falling; } 
	}
*/





