// musical shell/editor
// written on 1202412172.003209
// by dwrr 

// old: 
// example sine wave gneration in 
// sdl2 in c.
// dwrr
// written on 1202407276.155019
// 202407276.212254: a music sine synthesizer

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdocumentation-unknown-command"
#pragma clang diagnostic ignored "-Wpadded"
#include <SDL.h>
#pragma clang diagnostic pop

#include <iso646.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>

enum note_state { sustained, rising, falling };
static const int sample_count = 4096;
static const int sample_rate = 44100;
static const float f_sample_rate = 2.0f * (float) sample_rate;
static const float tau = (float) (2.0 * M_PI);
static const float start_vol = 0.00005f;
static const float cut_off_vol = 0.00001f;
static const float max_vol = 0.05f;
static const float start_speed = 0.003f;
static const float decay_speed = 0.00002f;

enum note_names { 
	C4, Db4, D4, Eb4, E4, F4, Gb4, G4, Ab4, A4, Bb4, B4,
	C5, Db5, D5, Eb5, E5, F5, Gb5, G5, Ab5, A5, Bb5, B5,
note_count
};
static const float freq[note_count] = {
	261.626f, 277.183f, 293.665f, 311.184f, 329.628f, 349.228f, 369.994f, 391.995f, 415.305f, 440.000f, 466.164f, 493.883f,
	2 * 261.626f, 2 * 277.183f, 2 * 293.665f, 2 * 311.184f, 2 * 329.628f, 2 * 349.228f, 2 * 369.994f, 2 * 391.995f, 2 * 415.305f, 2 * 440.000f, 2 * 466.164f, 2 * 493.883f,
};

struct note {
	float step;
	float volume;
};

static struct note notes[note_count] = {0};
static uint8_t state[note_count] = {0};

static struct termios terminal = {0};

static void oscillator_callback(
	void* userdata, Uint8* stream, int len
) {
	float* output = (float*) stream;
	for (int o = 0; o < sample_count * 2; o++) {
		output[o] = 0.0;
		for (int n = 0; n < note_count; n++) {

				output[o] += sinf(notes[n].step) * notes[n].volume;
				notes[n].step = fmodf(notes[n].step + tau / (f_sample_rate / (2 * freq[n])), tau);

			if (state[n] == falling) {

				if (notes[n].volume > cut_off_vol) 
					notes[n].volume *= (1 - decay_speed);
				else { 
					state[n] = sustained; 
					notes[n].volume = 0.0f; 
				}

			} else if (state[n] == rising) {

				if (notes[n].volume < max_vol) { 
					if (notes[n].volume < start_vol) 
						notes[n].volume += start_vol; 
					notes[n].volume *= (1 + start_speed); 
				} else { 
					state[n] = sustained; 
					notes[n].volume = max_vol; 
				}
			}
		}
	}
}

int main(void) {

	if (SDL_Init(SDL_INIT_AUDIO) < 0) {
		printf("SDL: %s\n", 
		SDL_GetError());
		exit(0);
	}

	SDL_AudioSpec spec = {
		.format = AUDIO_F32,
		.channels = 2,
		.freq = sample_rate,
		.samples = sample_count,
		.callback = oscillator_callback,
	};

	if (SDL_OpenAudio(&spec, NULL) < 0) {
		printf("%s\n", SDL_GetError());
		exit(1);
	}

	tcgetattr(0, &terminal);
	struct termios terminal_copy = terminal; 
	terminal_copy.c_cc[VMIN] = 1;
	terminal_copy.c_cc[VTIME] = 0;
	terminal_copy.c_lflag &= ~((size_t) (ICANON | ECHO));
	tcsetattr(0, TCSAFLUSH, &terminal_copy);

	SDL_PauseAudio(0);

	while (1) {
		char c = 0;
		read(0, &c, 1);
		if (c == 'q') break;
		if (c == 'a') { state[0] = rising; } 
		if (c == 'd') { state[0] = falling; } 
		if (c == 'r') { state[4] = rising; }  
		if (c == 't') { state[4] = falling; } 
		if (c == 'i') { state[7] = rising; } 
		if (c == 'p') { state[7] = falling; } 
		if (c == 'u') { state[11] = rising; } 
		if (c == 'n') { state[11] = falling; } 
		if (c == ' ') { state[0] = falling; } 
	}
	SDL_PauseAudio(1);
	exit(0);
}

































/*
copyb insert ./build
copya do ,./run


*/


//	const char key_rising[note_count]  = "1234567890-=ashtgyneoi";
//	const char key_falling[note_count] = "qdrwbjfup;[]zxmcvkl,./";


/*static const int melody[] = {
	D4, A4, F4, D4, A4, F4, D4, A4, F4, D4, A4, F4, 
	D4, Bb4, G4, D4, Bb4, G4, D4, Bb4, G4, D4, Bb4, G4, 
	Db4, G4, E4, Db4, G4, E4, Db4, Bb4, G4, E4, A4, G4, 
	F4, D4, F4, A4, F4, A4, D5, A4, D5, F5, D5, F5, 
	B5, F5, E5, D5, C5, B4, A4, Ab4, Gb4, E4, D5, B4, 
	C5, C5, 
};*/
//const int melody_count = sizeof melody / sizeof *melody;

//static int melody_index = 0; // melody_count;
//static int counter = 0;

/*
		if (melody_index < melody_count) {
			     if (counter == 0) { state[melody[melody_index]] = rising; counter++; }
			else if (counter ==  15000) { if (melody_index) state[melody[melody_index - 1]] = falling; counter++; }
			else if (counter >=  30000) { melody_index++; counter = 0; }
			else counter++;

		} else if (melody_index == melody_count) {
			     if (counter == 0) { counter++; }
			else if (counter ==  15000) { if (melody_index) state[melody[melody_index - 1]] = falling; counter++; }
			else if (counter >=  30000) { melody_index++; counter = 0; }
			else counter++;
		}
*/


	// harmonic series:
	//110, 220, 330, 440,  550, 660, 770, 880,  990, 1100, 1210, 1320, 
	//13 * 110, 14 * 110, 15 * 110, 16 * 110,  17 * 110, 18 * 110, 19 * 110, 20 * 110,  21 * 110, 22 * 110, 23 * 110, 24 * 110, 
	// equal temperment:




/*


		for (int i = 0; i < note_count; i++) {
			if (c == key_rising[i])  state[i] = rising;
			if (c == key_falling[i]) state[i] = falling;
		}






const int melody[] =   {C4,
		D4, A4, F4, D4, A4, F4, D4, A4, F4, D4, A4, F4, 
		D4, Bb4, G4, D4, Bb4, G4, D4, Bb4, G4, D4, Bb4, G4, 
		Db4, G4, E4, Db4, G4, E4, Db4, Bb4, G4, E4, A4, G4, 
		F4, D4, F4, A4, F4, A4, D5, A4, D5, F5, D5, F5, 
		B5, F5, E5, D5, C5, B4, A4, Ab4, Gb4, E4, D5, B4, 
		C5

	};

	//const int duration[] = { 1,   1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1, 1,  1,  1,  1,  1,  1,  1,  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, };

	const int melody_count = sizeof melody / sizeof *melody;

	for (int i = 0; i < melody_count - 1; i++) {

		state[melody[i + 1]] = rising;

		for (int d = 0; d < 1; d++) { // duration[i + 1]
			nanosleep((const struct timespec[]){{0, 2 * 40000000}}, NULL);
		}

		state[melody[i]] = falling;
		nanosleep((const struct timespec[]){{0, 2 * 150000000}}, NULL);
	}


	for (int i = 0; i < note_count; i++) state[i] = falling;
	nanosleep((const struct timespec[]){{2, 0}}, NULL);
	exit(0);




*/













































//	struct sigaction action2 = {.sa_handler = interrupted}; 
//	sigaction(SIGINT, &action2, NULL);













/*


		if (turning_off0) {
			if (_0.volume > cut_off_vol) _0.volume *= (1 - decay_speed);
			else { turning_off0 = 0; _0.volume = 0.0f; }
		} else if (turning_on0) {
			if (_0.volume < max_vol) { if (_0.volume < start_vol) _0.volume += start_vol; _0.volume *= (1 + start_speed); }
			else { turning_on0 = 0; _0.volume = max_vol; }
		}

		if (turning_off1) {
			if (_1.volume > cut_off_vol) _1.volume *= (1 - decay_speed);
			else { turning_off1 = 0; _1.volume = 0.0f; }
		} else if (turning_on1) {
			if (_1.volume < max_vol) { if (_1.volume < start_vol) _1.volume += start_vol; _1.volume *= (1 + start_speed); }
			else { turning_on1 = 0; _1.volume = max_vol; }
		}

		if (turning_off2) {
			if (_2.volume > cut_off_vol) _2.volume *= (1 - decay_speed);
			else { turning_off2 = 0; _2.volume = 0.0f; }
		} else if (turning_on2) {
			if (_2.volume < max_vol) { if (_2.volume < start_vol) _2.volume += start_vol; _2.volume *= (1 + start_speed); }
			else { turning_on2 = 0; _2.volume = max_vol; }
		}


*/





//static const float  A4_OSC = 2 * (float) sample_rate / (440.000f);
//static const float  E4_OSC = 2 * (float) sample_rate / (329.628f);
//static const float Cs4_OSC = 2 * (float) sample_rate / (277.183f);









/*
	for (int i = 0; i < sample_count * 2; i++) {

		const float f0 = sinf(notes[0].step) * notes[0].volume;
		const float f1 = sinf(notes[1].step) * notes[1].volume;
		const float f2 = sinf(notes[2].step) * notes[2].volume;

		output[i] = f0 + f1 + f2;

		notes[0].step = fmodf(notes[0].step + (2 * M_PI) / (2 * (float) sample_rate / (freq[0])), 2 * M_PI);
		notes[1].step = fmodf(notes[1].step + (2 * M_PI) / (2 * (float) sample_rate / (freq[1])), 2 * M_PI);
		notes[2].step = fmodf(notes[2].step + (2 * M_PI) / (2 * (float) sample_rate / (freq[2])), 2 * M_PI);

		if (state[0] == falling) {
			if (notes[0].volume > cut_off_vol) notes[0].volume *= (1 - decay_speed);
			else { state[0] = sustained; notes[0].volume = 0.0f; }
		} else if (state[0] == rising) {
			if (notes[0].volume < max_vol) { if (notes[0].volume < start_vol) notes[0].volume += start_vol; notes[0].volume *= (1 + start_speed); }
			else { state[0] = 0; notes[0].volume = max_vol; }
		}

		if (state[1] == falling) {
			if (notes[1].volume > cut_off_vol) notes[1].volume *= (1 - decay_speed);
			else { state[1] = sustained; notes[1].volume = 0.0f; }
		} else if (state[1] == rising) {
			if (notes[1].volume < max_vol) { if (notes[1].volume < start_vol) notes[1].volume += start_vol; notes[1].volume *= (1 + start_speed); }
			else { state[1] = 0; notes[1].volume = max_vol; }
		}

		if (state[2] == falling) {
			if (notes[2].volume > cut_off_vol) notes[2].volume *= (1 - decay_speed);
			else { state[2] = sustained; notes[2].volume = 0.0f; }
		} else if (state[2] == rising) {
			if (notes[2].volume < max_vol) { if (notes[2].volume < start_vol) notes[2].volume += start_vol; notes[2].volume *= (1 + start_speed); }
			else { state[2] = 0; notes[2].volume = max_vol; }
		}
	}

*/








//277.183f, 329.628f, 440.000f, 




//const int duration[] = { 1,   1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1, 1,  1,  1,  1,  1,  1,  1,  1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, };





//nanosleep((const struct timespec[]){{0, 2 * 150000000}}, NULL);
	//nanosleep((const struct timespec[]){{0, 2 * 40000000}}, NULL);
	//nanosleep((const struct timespec[]){{2, 0}}, NULL);
	//exit(0);



